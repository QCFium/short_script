#pragma once

struct Variable;
class Expression;
using op_func_t = void(Variable &, const Variable &);

struct Operator {
	char symbol;
	op_func_t *function;
	bool is_binary;
	bool is_advanced; // if it starts with '$'
	size_t pos = -1;
	bool operator == (const Operator &op) const {
		return symbol == op.symbol && is_binary == op.is_binary && is_advanced == op.is_advanced;
	}
	bool operator != (const Operator &op) const { return !(*this == op); }
};

#define OP_ADD {'+', &Operators::add, true, false}
#define OP_SUB {'-', &Operators::sub, true, false}
#define OP_MUL {'*', &Operators::mul, true, false}
#define OP_DIV {'/', &Operators::div, true, false}
#define OP_REM {'%', &Operators::rem, true, false}
#define OP_AND {'&', &Operators::and_, true, false}
#define OP_OR  {'|', &Operators::or_,  true, false}
#define OP_XOR {'^', &Operators::xor_, true, false}
#define OP_GT {'>', &Operators::greater, true, false}
#define OP_LT {'<', &Operators::less,    true, false}
#define OP_EQ {'=', &Operators::equal,   true, false}
#define OP_COMMA {',', &Operators::comma,  true, false}
#define OP_QUOT {'\'', &Operators::quot,   true, false}

#define OP_NEG {'-', &Operators::neg, false, false}
#define OP_NOT {'!', &Operators::inv, false, false}
#define OP_ABS {'+', &Operators::abs, false, false}
#define OP_NYORO {'~', &Operators::nyoro, false, false}

#define OP_DECODE_CHAR {'d', &Operators::decode_char, false, true}

#define OP_MAX {'>', &Operators::select_max, true, true}
#define OP_MIN {'<', &Operators::select_min, true, true}
#define OP_LOWER_BOUND {'l', &Operators::lower_bound, true, true}
#define OP_UPPER_BOUND {'u', &Operators::upper_bound, true, true}
#define OP_COUNT {'c', &Operators::count, true, true}

extern const std::vector<std::vector<Operator> > op_groups;
extern const std::vector<Operator> operators;
extern const std::vector<op_func_t *> ptr_supported; // operators supporting TYPE_PTR as argument

bool operator < (const Variable &, const Variable &);
bool operator > (const Variable &, const Variable &);
bool operator == (const Variable &, const Variable &);
bool operator != (const Variable &, const Variable &);

namespace Operators {
	// normal binary operators
	op_func_t add;
	op_func_t sub;
	op_func_t mul;
	op_func_t div;
	op_func_t rem;
	op_func_t and_;
	op_func_t or_;
	op_func_t xor_;
	op_func_t greater;
	op_func_t less;
	op_func_t equal;
	op_func_t array_get;
	op_func_t comma;
	op_func_t quot;
	op_func_t nyoro;
	
	// prefix unary operators
	op_func_t neg;
	op_func_t inv;
	op_func_t abs;
	// suffix unary operators
	op_func_t decode_char;
	
	// advanced binary operators
	op_func_t select_max;
	op_func_t select_min;
	op_func_t lower_bound;
	op_func_t upper_bound;
	op_func_t count;
}
