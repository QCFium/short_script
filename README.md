## ShortScript
A script language designed to be written super shortly.  
Refer doc/Tutorial.md for language tutorial, doc/Specifications.md for strict language specifications.  
You need a shell that supports ANSI escape code(most of UNIX shells, cmder for Windows and cmd.exe in recent Windows10) to see error messages properly.

#### Build
run `make` to build binary \(named out\(.exe\)\)

#### License
You may use this under the terms of the GNU General Public License GPL v2 or under the terms of any later revisions of the GPL. Refer to the provided LICENSE file for further information.
