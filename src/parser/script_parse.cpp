#include <limits>
#include <cassert>
#include "short_script.h"
#include "structure/sort.h"

void ShortScript::compile_loop(std::vector<CompiledStatement> &res, std::vector<ParseBlock> &blocks) {
	assert(source[parse_head] == '{');
	size_t loop_start = parse_head;
	if (parse_head + 1 >= size)
		throw Expected("counter lvalue, comparator or expression", -1);
	parse_head++;
	
	size_t counter_pos;
	
	size_t counter_end = parser.parse_lvalue(parse_head, nullptr);
	
	// skip counter lvalue
	if (counter_end != (size_t) -1) {
		counter_pos = parse_head;
		size_t head = counter_end;
		if (head >= size) throw Expected("':', comparator or expression", -1);
		if (source[head] == ':') parse_head = head + 1;
		else counter_pos = -1; // that's not a counter lvalue
	} else counter_pos = -1;
	
	if (parse_head >= size) throw Expected("comparator or expression", -1);
	
	// type2 loop
	if (source[parse_head] == '|') {
		parse_head++;
		size_t target_pos = parse_head;
		parse_head = parser.parse_rvalue(parse_head, (Variable**) nullptr, false, true, true);
		assert(parse_head > target_pos);
		blocks.push_back(ParseBlock(ParseBlock::TYPE_FOR, res.size()));
		res.push_back(CompiledStatement(CompiledStatement::TYPE_FOR_ITR, loop_start, -1, counter_pos));
		res.back().for_start = target_pos;
		return;
	}
	// properties
	size_t start_pos = -1;
	size_t finish_pos = -1;
	bool down = false;
	bool open = false;
	
	// read the start expression
	if (source[parse_head] != '<' &&
		source[parse_head] != '>' &&
		source[parse_head] != ')') {
		// get the end of the expression without '>' or '<'
		size_t end = parser.parse_rvalue(parse_head, (Variable**) nullptr, false, true, false);
		assert(end > parse_head);
		if (end < size && source[end] == '}')
			throw SyntaxError("Single expression inside {} is not supported", end); // maybe something in future
		if (end < size && source[end] == ')') {
			// {y)} which means [0,y) so y is finish expression, not start one
		} else {
			start_pos = parse_head;
			parse_head = end;
		}
	} else {}; // no start expression(defaults 0)
	
	// read comparator
	if (parse_head >= size) throw Expected("comparator or finish expression", -1);
	if (source[parse_head] == '<' || source[parse_head] == '>') {
		down = (source[parse_head] == '>');
		parse_head++;
	}
	
	// read finish expression
	if (parse_head >= size) throw Expected("finish expression or '}'", -1);
	if (source[parse_head] != ')') {
		finish_pos = parse_head;
		parse_head = parser.parse_rvalue(parse_head, (Variable**) nullptr, false, true, true);
		assert(parse_head > finish_pos);
	} else {}; // no finish expression
	
	if (source[parse_head] == ')') open = true, parse_head++;
	
	blocks.push_back(ParseBlock(ParseBlock::TYPE_FOR, res.size()));
	res.push_back(CompiledStatement(CompiledStatement::TYPE_FOR, loop_start, counter_pos, start_pos, finish_pos, down, open));
}

void ShortScript::compile_statement(std::vector<CompiledStatement> &res) {
	std::vector<ParseBlock> blocks;
	parse_head = 0;
	while (parse_head < size || blocks.size()) {
		// block handling(if, loop etc)
		if (blocks.size()) {
			ParseBlock &top_block = blocks.back();
			if (top_block.type == ParseBlock::TYPE_IF) {
				if (parse_head >= size || source[parse_head] == '}') {
					parse_head++;
					assert(!top_block.continues.size());
					for (auto i : top_block.breaks) {
						assert(i < res.size());
						assert(res[i].type == CompiledStatement::TYPE_JUMP);
						res[i].target = res.size();
					}
					if (res[top_block.index].target == (size_t) -1) res[top_block.index].target = res.size();
					blocks.pop_back();
					continue;
				}
				if (source[parse_head] == ':') {
					if (top_block.is_main) {
						top_block.is_main = false;
						top_block.breaks.push_back(res.size());
						res.push_back(CompiledStatement(CompiledStatement::TYPE_JUMP, parse_head, -1));
						res[top_block.index].target = res.size();
						parse_head++;
					} else { // implicitly end of 'if' block
						// the ':' is closing another conditonal block so no parse_head++
						assert(!top_block.continues.size());
						for (auto i : top_block.breaks) {
							assert(i < res.size());
							assert(res[i].type == CompiledStatement::TYPE_JUMP);
							res[i].target = res.size();
						}
						blocks.pop_back();
						continue;
					}
				}
			} else if (top_block.type == ParseBlock::TYPE_FOR) {
				if (parse_head >= size || source[parse_head] == '}') { // end of loop block
					parse_head++;
					for (auto i : top_block.continues) {
						assert(i < res.size());
						assert(res[i].type == CompiledStatement::TYPE_JUMP);
						res[i].target = res.size();
					}
					res.push_back(CompiledStatement(CompiledStatement::TYPE_END, -1, top_block.index));
					for (auto i : top_block.breaks) {
						assert(i < res.size());
						assert(res[i].type == CompiledStatement::TYPE_JUMP);
						res[i].target = res.size();
					}
					res[top_block.index].target = res.size();
					blocks.pop_back();
					continue;
				}
			} else assert(!"Unexpected type in compile(Internal Error)");
		}
		
		while (parse_head < size && isspace(source[parse_head])) parse_head++; // skip whitespaces
		if (parse_head >= size) break;
		size_t statement_start = parse_head;
		try {
			// actual run
			// loop statement({} is an empty set literal)
			if (source[parse_head] == '{' && parse_head + 1 < size && source[parse_head + 1] != '}') {
				compile_loop(res, blocks);
				continue;
			}
			// special statement
			if (source[parse_head] == '\\') {
				parse_head++;
				if (parse_head >= size) throw Expected("special statement specifier after '\\'", -1);
				if (source[parse_head] == 'c' || source[parse_head] == 'b') {
					res.push_back(CompiledStatement(CompiledStatement::TYPE_SPECIAL, parse_head - 1, -1));
					parse_head++;
				} else throw SyntaxError("Unknown escaped char for special statement:"s + source[parse_head], parse_head);
				continue;
			}
			size_t exp_end = parser.parse_rvalue(parse_head, (Variable**) nullptr, false, false, true);
			bool is_end = exp_end >= size;
			if (!is_end) for (auto c : ":}"s)
				if (c == source[exp_end]) is_end = true;
			
			if (!is_end && source[exp_end] == '?') { // 'if' statement
				blocks.push_back(ParseBlock(ParseBlock::TYPE_IF, res.size()));
				res.push_back(CompiledStatement(CompiledStatement::TYPE_IF, parse_head, -1));
				parse_head = exp_end + 1;
				continue;
			}
			
			size_t lvalue_end = parser.parse_lvalue(parse_head, nullptr);
			if (!is_end && lvalue_end != (size_t) -1) {
				if (source[exp_end] == '#') {
					// type2 operation
					res.push_back(CompiledStatement(CompiledStatement::TYPE_COMPOUND, parse_head, -1));
					parse_head = exp_end;
					Lvalue dummy;
					run_compound_operation(dummy, false); // skip arguments
					continue;
				} else {
					// assign statement
					size_t right_hand_end = parser.parse_rvalue(exp_end, (Variable**) nullptr, true, true, true);
					if (right_hand_end != (size_t) -1 && right_hand_end > exp_end) { // there is right hand
						res.push_back(CompiledStatement(CompiledStatement::TYPE_ASSIGN, parse_head, -1));
						parse_head = right_hand_end;
						continue;
					} // else: failed to parse the right hand side, that's not an assign statement
				}
			}
			// else : output statement
			res.push_back(CompiledStatement(CompiledStatement::TYPE_OUTPUT, parse_head, -1));
			parse_head = exp_end;
		} catch (const RuntimeError &error) {
			parser.stack_trace.push({"While compiling the statement starting at here", statement_start});
			throw;
		}
	}
	if (show_compile_result) {
		for (size_t i = 0; i < res.size(); i++) {
			std::cerr << i << ":";
			res[i].output(stderr);
		}
	}
}

bool ShortScript::run_statements(const std::vector<CompiledStatement> &statements) {
	static const Variable plus1(1LL);
	static const Variable minus1(-1LL);
	static Variable *tmp_rvalue;
	static const Variable *tmp_const_rvalue;
	static Lvalue *tmp_lvalue;
	size_t index = 0;
	std::vector<Block> blocks; // stores the index(in 'statements') of block start
	while (index < statements.size()) {
		const CompiledStatement &cur_statement = statements[index];
		try {
			if (cur_statement.type == CompiledStatement::TYPE_ASSIGN) {
				size_t left_end = parser.parse_lvalue(cur_statement.start, &tmp_lvalue);
				parser.parse_rvalue(left_end, &tmp_rvalue, false, true, true);
				if (tmp_rvalue->type == Variable::Type::PTR) tmp_lvalue->set(parser, *tmp_rvalue->ptr);
				else tmp_lvalue->set(parser, std::move(*tmp_rvalue));
				index++;
			} else if (cur_statement.type == CompiledStatement::TYPE_OUTPUT) {
				parser.parse_rvalue(cur_statement.start, &tmp_const_rvalue, false, true, true);
				tmp_const_rvalue->output();
				index++;
			} else if (cur_statement.type == CompiledStatement::TYPE_FOR) {
				// start new block if not yet
				if (!blocks.size() || blocks.back().index != index) {
					Lvalue _counter;
					Lvalue *counter = &_counter;
					if (cur_statement.counter_pos == (size_t) -1) _counter.assign_new_counter(parser);
					else parser.parse_lvalue(cur_statement.counter_pos, &counter);
					
					// set initial counter
					if (cur_statement.for_start != (size_t) -1) {
						parser.parse_rvalue(cur_statement.for_start, &tmp_rvalue, false, true, false);
						if (tmp_rvalue->type == Variable::Type::PTR) counter->set(parser, *tmp_rvalue->ptr);
						else counter->set(parser, std::move(*tmp_rvalue));
					} else {
						static const Variable initial_default(0LL);
						counter->set(parser, initial_default);
					}
					
					// read finish
					static const Variable _finish(std::numeric_limits<long long>::max());
					const Variable *finish = &_finish;
					if (cur_statement.for_finish != (size_t) -1)
						parser.parse_rvalue(cur_statement.for_finish, &finish, false, true, true);
					
					blocks.push_back(Block(index, *counter, *finish, cur_statement.for_open, cur_statement.for_down));
				}
				// condition check
				Block &block = blocks.back();
				bool down = cur_statement.for_down;
				bool finish =
					(!down && block.counter.get_complete_lvalue(parser) > block.finish) ||
					(down && block.counter.get_complete_lvalue(parser) < block.finish) ||
					(cur_statement.for_open && block.counter.get_complete_lvalue(parser) == block.finish);
				
				if (finish) {
					index = statements[block.index].target;
					block.counter.free(parser);
					blocks.pop_back();
				} else index++;
			} else if (cur_statement.type == CompiledStatement::TYPE_FOR_ITR) {
				// start new block if not yet
				if (!blocks.size() || blocks.back().index != index) {
					Lvalue _counter;
					Lvalue *counter = &_counter;
					if (cur_statement.counter_pos == (size_t) -1) _counter.assign_new_counter(parser);
					else parser.parse_lvalue(cur_statement.counter_pos, &counter);
					
					// get all of parent
					std::vector<Variable> all;
					const Variable *parent;
					// for_start points the parent
					parser.parse_rvalue(cur_statement.for_start, &parent, false, true, true);
					parent->get_all(all);
					
					if (!all.size()) { // nothing to iterate
						index = cur_statement.target;
						continue;
					}
					
					blocks.push_back(Block(index, *counter, std::move(all)));
				}
				
				// set counter / condition check
				Block &block = blocks.back();
				if (block.itr_pos >= block.list.size()) {
					index = statements[block.index].target;
					block.counter.free(parser);
					blocks.pop_back();
				} else {
					// set counter
					block.counter.set(parser, block.list[block.itr_pos]);
					index++;
				}
			} else if (cur_statement.type == CompiledStatement::TYPE_IF) {
				parser.parse_rvalue(cur_statement.start, &tmp_const_rvalue, false, false, true);
				if (!(bool) *tmp_const_rvalue) index = cur_statement.target;
				else index++;
			} else if (cur_statement.type == CompiledStatement::TYPE_END) { // end of loop statement
				assert(cur_statement.target < statements.size());
				assert(blocks.size());
				Block &block = blocks.back();
				if (block.type == Block::TYPE_FOR) {
					// increment or decrement
					Operators::add(block.counter.get_complete_lvalue(parser),
						block.down ? minus1 : plus1); // add 1 or -1
				} else if (block.type == Block::TYPE_FOR_ITR) {
					block.itr_pos++;
				} else assert(0);
				index = cur_statement.target;
			} else if (cur_statement.type == CompiledStatement::TYPE_JUMP) index = cur_statement.target;
			else if (cur_statement.type == CompiledStatement::TYPE_COMPOUND) {
				parse_head = parser.parse_lvalue(cur_statement.start, &tmp_lvalue);
				run_compound_operation(*tmp_lvalue, true);
				index++;
			} else if (cur_statement.type == CompiledStatement::TYPE_SPECIAL) {
				char specifier = source[cur_statement.start + 1];
				if (specifier == 'b' || specifier == 'c') {
					if (!blocks.size()) {
						index = statements.size(); // jump to the end of the script
						parse_head = size;
					} else if (specifier == 'b') {
						assert(blocks.size());
						Block &block = blocks.back();
						index = statements[block.index].target;
						block.counter.free(parser);
						blocks.pop_back();
					} else if (specifier == 'c') {
						assert(blocks.size());
						index = statements[blocks.back().index].target - 1;
					}
				} else assert(0);
			} else assert(0);
		} catch (const RuntimeError &error) {
			parser.stack_trace.push({"In execution of the statement starting at here", cur_statement.start});
			throw;
		}
	}
	return true;
}
