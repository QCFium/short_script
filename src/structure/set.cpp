#include <iostream>
#include <cassert>
#include "set.h"
#include "variable.h"
#include "error/error.h"

Set & Set::operator = (const Set &set) {
	if (this != &set) {
		if (!set.integer_set || !set.integer_set->size()) this->integer_set = nullptr;
		else {
			delete this->integer_set;
			this->integer_set = new Set_Integer(*set.integer_set);
			if (!this->integer_set) throw RuntimeError("Failed to allocate memory in Set of Integer assignment");
		}
		if (!set.string_set || !set.string_set->size()) this->string_set = nullptr;
		else {
			delete this->string_set;
			this->string_set = new Set_String(*set.string_set);
			if (!this->string_set) throw RuntimeError("Failed to allocate memory in Set of String assignment");
		}
	}
	return *this;
}
Set & Set::operator = (Set &&set) {
	if (this != &set) {
		std::swap(integer_set, set.integer_set);
		std::swap(string_set, set.string_set);
		std::swap(is_multi, set.is_multi);
	}
	return *this;
}
Set::~Set() {
	delete integer_set;
	delete string_set;
	integer_set = nullptr;
	string_set = nullptr;
}
size_t Set::size() const {
	if (integer_set) return integer_set->size();
	else if (string_set) return string_set->size();
	else return 0;
}
void Set::set_multi(bool multi) {
	this->is_multi = multi;
	if (integer_set) integer_set->set_multi(multi);
	if (string_set) string_set->set_multi(multi);
}
Variable Set::operator[] (long long index) const {
	if (index < 0 || ((size_t)index) >= size()) throw RangeError("Set", size(), index);
	if (integer_set) return Variable((*integer_set)[index]);
	else if (string_set) return Variable((*string_set)[index]);
	throw RangeError("Set", 0, index);
}
Set &Set::operator &= (const Set &set) {
	std::vector<Variable> all;
	this->get_all(all);
	for (auto &var : all) if (!set.count(var)) this->remove(var);
	return *this;
}
Set &Set::operator |= (const Set &set) {
	std::vector<Variable> all;
	set.get_all(all);
	for (auto &var : all) this->insert(var);
	return *this;
}
Set &Set::operator ^= (const Set &set) {
	std::vector<Variable> all_set;
	set.get_all(all_set);
	for (auto &var : all_set) {
		if (this->count(var)) this->remove(var);
		else this->insert(var);
	}
	return *this;
}
Set &Set::operator -= (const Set &set) {
	std::vector<Variable> all_set;
	set.get_all(all_set);
	for (auto &var : all_set) this->remove(var);
	return *this;
}
void Set::insert(Variable var) {
	if (integer_set && integer_set->size()) {
		integer_set->insert((long long) var);
	} else if (string_set && string_set->size()) {
		string_set->insert((std::string) var);
	} else { // type free
		if (var.type == Variable::Type::INTEGER) {
			if (!integer_set) integer_set = new Set_Integer(is_multi);
			integer_set->insert(var.integer);
		} else if (var.type == Variable::Type::STRING) {
			if (!string_set) string_set = new Set_String(is_multi);
			string_set->insert(var.str);
		} else throw RuntimeError("Invalid type for Set : " + type_to_str(var.type));
	}
}
void Set::remove(Variable n) {
	if (integer_set && integer_set->size()) {
		integer_set->remove((long long) n);
		if (!integer_set->size()) {
			delete integer_set;
			integer_set = nullptr;
		}
	} else if (string_set && string_set->size()) {
		string_set->remove((std::string) n);
		if (!string_set->size()) {
			delete string_set;
			string_set = nullptr;
		}
	} else {} // empty set
}
void Set::remove_index(long long index) {
	if (index < 0 || ((size_t)index) >= size()) throw RangeError("Set", size(), index);
	if (integer_set) {
		integer_set->remove_index(index);
		if (!integer_set->size()) {
			delete integer_set;
			integer_set = nullptr;
		}
	} else if (string_set) {
		string_set->remove_index(index);
		if (!string_set->size()) {
			delete string_set;
			string_set = nullptr;
		}
	} else throw RangeError("Set", 0, index);
}
size_t Set::count(Variable n) const {
	if (integer_set && integer_set->size()) {
		return integer_set->count((long long) n);
	} else if (string_set && string_set->size()) {
		return string_set->count((std::string) n);
	} else { // empty set
		return 0;
	}
}
size_t Set::lower_bound(const Variable &var) const {
	if (!size()) return 0;
	if (integer_set) {
		return integer_set->lower_bound((long long) var);
	} else if (string_set) {
		return string_set->lower_bound((std::string) var);
	} else assert(!"Set::lower_bound : size() > 0 but both integer_set and string_set is invalid");
}
size_t Set::upper_bound(const Variable &var) const {
	if (!size()) return 0;
	if (integer_set) {
		return integer_set->upper_bound((long long) var);
	} else if (string_set) {
		return string_set->upper_bound((std::string) var);
	} else assert(!"Set::upper_bound : size() > 0 but both integer_set and string_set is invalid");
}
void Set::get_all(std::vector<Variable> &result) const {
	if (integer_set) integer_set->get_all(result);
	else if (string_set) string_set->get_all(result);
	else result = {};
}
void Set::output(FILE *stream) const {
	if (integer_set) integer_set->output(stream);
	else if (string_set) string_set->output(stream);
	else {} // it's an empty set
}
