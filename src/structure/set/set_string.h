#pragma once
#include <vector>
#include <string>
#include <cstdio>

struct Variable;

// implemented as trie
class Set_String {
	// unsigned char(0~255) is expressed in 8-bit
	struct Node {
		size_t left;
		size_t right;
		size_t parent;
		size_t subtree_size; // size of subtree
		size_t subtree_size_unique;
		size_t get_size(bool unique) const { return unique ? subtree_size_unique : subtree_size; }
		explicit Node(size_t parent = -1) : parent(parent) {
			left = (size_t) -1;
			right = (size_t) -1;
			subtree_size = 0;
			subtree_size_unique = 0;
		}
	};
	std::vector<Node> nodes;
	size_t _get_self_size(size_t node) const; // example : if node points "ab", exclude "abc"
	size_t _count_lower(const std::string &str) const;
	size_t _index_to_node_index(size_t index) const;
	
	size_t _size = 0;
	size_t _size_unique = 0;
public:
	
	bool unique = true; // false if it's multiset
	Set_String(bool is_multi);
	void set_multi(bool);
	size_t size(void) const;
	std::string operator[] (size_t index) const;
	void insert(const std::string &, size_t num = 1);
	void remove(const std::string &); // remove one if there are multiple
	void remove_index(size_t index); // same as above
	size_t count(const std::string &) const;
	size_t lower_bound(const std::string &) const;
	size_t upper_bound(const std::string &) const;
	void get_all(std::vector<Variable> &result) const;
	void get_all_map(std::vector<std::pair<std::string, size_t> > &) const; // internally used
	void output(FILE *stream = stdout) const;
};
