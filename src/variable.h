#pragma once
#include <string>
#include "structure/set.h"
#include "structure/advanced_array/advanced_array.h"

using namespace std::literals::string_literals;

struct Variable {
	union {
		long long integer;
		double floating;
		std::string str;
		std::vector<Variable> array;
		AdvancedArray advanced_array;
		Set set;
		const Variable *ptr;
	};
	
	enum class Type {
		NONE,
		INTEGER,
		FLOAT,
		STRING,
		ARRAY,
		ADVANCED_ARRAY,
		SET,
		PTR
	} type = Type::NONE;
	
	explicit Variable(long long);
	explicit Variable(double);
	explicit Variable(const std::string &);
	explicit Variable(const std::vector<Variable> &);
	explicit Variable(const AdvancedArray &);
	explicit Variable(const Set &);
	explicit Variable(const Variable *); // init as pointer to another variable for faster copying
	Variable(const Variable &);
	Variable(Variable &&) noexcept;
	Variable(void) {};
	~Variable(void);
	
	Variable & operator = (const Variable &);
	Variable & operator = (Variable &&) noexcept;
	
	explicit operator double () const;
	explicit operator long long () const;
	explicit operator bool () const;
	explicit operator std::string () const;
	explicit operator std::vector<Variable> () const;
	explicit operator AdvancedArray () const;
	explicit operator Set () const;
	void cast(enum Type, Variable &res) const;
	
	void get_all(std::vector<Variable> &result) const;
	
	bool is_primitive() const;
	size_t depth() const;
	
	void array_set(const std::vector<std::pair<size_t, Variable> > &, const Variable &);
	
	void output(FILE * = stdout) const;
};

std::string type_to_str(Variable::Type);

