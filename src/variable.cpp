#include <vector>
#include <climits>
#include <cassert>
#include <iostream>
#include "variable.h"
#include "error/error.h"
#include "utils.h"

using namespace std::literals::string_literals;

std::string type_to_str(Variable::Type type) {
	if (type == Variable::Type::INTEGER) return "Integer";
	if (type == Variable::Type::FLOAT) return "Float";
	if (type == Variable::Type::STRING) return "String";
	if (type == Variable::Type::ARRAY) return "Array";
	if (type == Variable::Type::ADVANCED_ARRAY) return "Advanced Array";
	if (type == Variable::Type::NONE) return "Null";
	if (type == Variable::Type::SET) return "Set";
	if (type == Variable::Type::PTR) return "Pointer(Internal type)";
	throw Fatal("Unknown type : "s + std::to_string((int)type), -1);
}

//////// constructers/destructors
Variable::Variable(long long n) {
	this->type = Type::INTEGER;
	this->integer = n;
}
Variable::Variable(double n) {
	this->type = Type::FLOAT;
	this->floating = n;
}
Variable::Variable(const std::string &str) {
	this->type = Type::STRING;
	new(&this->str) std::string(str);
}
Variable::Variable(const std::vector<Variable> &array) {
	this->type = Type::ARRAY;
	new(&this->array) std::vector<Variable> (array);
}
Variable::Variable(const AdvancedArray &tree) {
	this->type = Type::ADVANCED_ARRAY;
	new(&this->advanced_array) AdvancedArray(tree);
}
Variable::Variable(const Set &set) {
	this->type = Type::SET;
	new(&this->set) Set(set);
}
Variable::Variable(const Variable *ptr) {
	this->type = Type::PTR;
	this->ptr = ptr;
}
Variable::Variable(const Variable &var) {
	if (var.type == Type::NONE) return;
	else if (var.type == Type::INTEGER) integer = var.integer;
	else if (var.type == Type::FLOAT) floating = var.floating;
	else if (var.type == Type::STRING) new(&this->str) std::string(var.str);
	else if (var.type == Type::ARRAY) new(&this->array) std::vector<Variable> (var.array);
	else if (var.type == Type::ADVANCED_ARRAY) new(&this->advanced_array) AdvancedArray(var.advanced_array);
	else if (var.type == Type::SET) new(&this->set) Set(var.set);
	else if (var.type == Type::PTR) ptr = var.ptr;
	type = var.type;
}
Variable::Variable(Variable &&var) noexcept {
	if (var.type == Type::NONE) return;
	else if (var.type == Type::INTEGER) integer = var.integer;
	else if (var.type == Type::FLOAT) floating = var.floating;
	else if (var.type == Type::STRING) new(&this->str) std::string(std::move(var.str));
	else if (var.type == Type::ARRAY) new(&this->array) std::vector<Variable> (std::move(var.array));
	else if (var.type == Type::ADVANCED_ARRAY) new(&this->advanced_array) AdvancedArray(var.advanced_array);
	else if (var.type == Type::SET) new(&this->set) Set(std::move(var.set));
	else if (var.type == Type::PTR) ptr = var.ptr;
	type = var.type;
}
Variable::~Variable() {
	if (type == Type::NONE) return;
	if (type == Type::STRING) str.~basic_string<char>();
	else if (type == Type::ARRAY) array.~vector<Variable>();
	else if (type == Type::ADVANCED_ARRAY) advanced_array.~AdvancedArray();
	else if (type == Type::SET) set.~Set();
}
////////


Variable & Variable::operator = (const Variable & var) {
	this->~Variable();
	if (&var != this) {
		if (var.type == Type::INTEGER) integer = var.integer;
		if (var.type == Type::FLOAT) floating = var.floating;
		if (var.type == Type::STRING) new(&this->str) std::string(var.str);
		if (var.type == Type::ARRAY) new(&this->array) std::vector<Variable> (var.array);
		if (var.type == Type::ADVANCED_ARRAY) new(&this->advanced_array) AdvancedArray(var.advanced_array);
		if (var.type == Type::SET) new(&this->set) Set(var.set);
		if (var.type == Type::PTR) ptr = var.ptr;
		type = var.type;
	}
	return *this;
}
Variable & Variable::operator = (Variable &&var) noexcept {
	this->~Variable();
	if (&var != this) {
		if (var.type == Type::INTEGER) integer = var.integer;
		if (var.type == Type::FLOAT) floating = var.floating;
		if (var.type == Type::STRING) new(&this->str) std::string(std::move(var.str));
		if (var.type == Type::ARRAY) new(&this->array) std::vector<Variable> (std::move(var.array));
		if (var.type == Type::ADVANCED_ARRAY) new(&this->advanced_array) AdvancedArray(std::move(var.advanced_array));
		if (var.type == Type::SET) new(&this->set) Set(std::move(var.set));
		if (var.type == Type::PTR) ptr = var.ptr;
		type = var.type;
	}
	return *this;
}


//////// cast operators
Variable::operator double () const {
	if (type == Type::INTEGER) return integer;
	if (type == Type::FLOAT) return floating;
	throw CastError(type, Type::FLOAT);
}
Variable::operator long long () const {
	if (type == Type::INTEGER) return integer;
	if (type == Type::FLOAT) return floating;
	if (type == Type::STRING) return str.size();
	if (type == Type::ARRAY) return array.size();
	if (type == Type::ADVANCED_ARRAY) return advanced_array.size();
	if (type == Type::SET) return set.size();
	throw CastError(type, Type::INTEGER);
}
Variable::operator bool () const {
	return (long long) *this;
}
Variable::operator std::string () const {
	if (type == Type::STRING) return str;
	if (type == Type::INTEGER) return std::to_string(integer);
	if (type == Type::FLOAT) {
		char res[256];
		snprintf(res, 256, "%.9f", floating);
		return std::string(res);
	}
	if (type == Type::NONE) return "(null)";
	throw CastError(type, Type::STRING);
}
Variable::operator std::vector<Variable> () const {
	if (type == Type::STRING) {
		std::vector<Variable> res(str.size());
		for (size_t i = 0; i < str.size(); i++)
			res[i] = Variable((long long) str[i]);
		return res;
	}
	if (type == Type::ARRAY) return array;
	if (type == Type::ADVANCED_ARRAY) {
		std::vector<Variable> res;
		advanced_array.get_all(res);
		return res;
	}
	if (type == Type::INTEGER) {
		if (integer < 0) throw RuntimeError("Creating an array with negative size : "s + std::to_string(integer));
		return std::vector<Variable>(integer, Variable(0LL));
	}
	if (type == Type::SET) {
		std::vector<Variable> res;
		set.get_all(res);
		return res;
	}
	throw CastError(type, Type::ARRAY);
}
Variable::operator AdvancedArray () const {
	if (type == Type::ARRAY)
		return AdvancedArray(array);
	if (type == Type::ADVANCED_ARRAY) return advanced_array;
	if (type == Type::STRING) {
		std::vector<Variable> all;
		for (size_t i = 0; i < str.size(); i++)
			all[i] = Variable((long long) str[i]);
		return AdvancedArray(all);
	}
	if (type == Type::INTEGER) {
		if (integer < 0) throw RuntimeError("Creating an advanced array with negative size : "s + std::to_string(integer));
		return AdvancedArray(integer, Variable(0LL));
	}
	if (type == Type::SET) {
		std::vector<Variable> all;
		set.get_all(all);
		return AdvancedArray(all);
	}
	throw CastError(type, Type::ADVANCED_ARRAY);
}
Variable::operator Set() const {
	if (type == Type::SET) return set;
	if (type == Type::ARRAY) {
		Set result;
		for (auto var : array) result.insert(var);
		return result;
	}
	if (type == Type::ADVANCED_ARRAY) {
		size_t size = advanced_array.size();
		Set res;
		for (size_t i = 0; i < size; i++)
			res.insert(advanced_array[i]);
		return res;
	}
	if (type == Type::INTEGER) {
		return Set((int)integer);
	}
	throw CastError(type, Type::SET);
}
void Variable::cast(enum Type type, Variable &res) const {
	if (type == this->type) res = *this;
	else if (type == Type::INTEGER) res =  Variable((long long) *this);
	else if (type == Type::FLOAT) res = Variable((double) *this);
	else if (type == Type::STRING) res = Variable((std::string) *this);
	else if (type == Type::ARRAY) res = Variable((std::vector<Variable>) *this);
	else if (type == Type::ADVANCED_ARRAY) res = Variable((AdvancedArray) *this);
	else if (type == Type::SET) res = Variable((Set) *this);
	else assert(!"Unexpected type");
}
////////

bool Variable::is_primitive() const {
	return type == Type::INTEGER || type == Type::FLOAT;
}
size_t Variable::depth() const {
	if (type == Type::ARRAY) return array.size() ? array[0].depth() + 1 : 1;
	if (type == Type::ADVANCED_ARRAY) return advanced_array.size() ? array[0].depth() + 1 : 1;
	return 0;
}
void Variable::get_all(std::vector<Variable> &result) const {
	if (type == Type::ARRAY) result = array;
	else if (type == Type::ADVANCED_ARRAY) {
		advanced_array.get_all(result);
	} else if (type == Type::STRING) {
		result.resize(str.size());
		for (size_t i = 0; i < str.size(); i++)
			result[i] = Variable((long long) str[i]);
	} else if (type == Type::INTEGER) {
		result.clear();
		for (size_t i = 0; i < CHAR_BIT * sizeof(long long) - 1; i++)
			if (((unsigned long long) integer) >> i & 1) result.push_back(Variable(1LL << i));
	} else if (type == Type::SET) {
		set.get_all(result);
	} else throw RuntimeError("Uniteratable type of variable : "s + type_to_str(type));
}

void Variable::array_set(const std::vector<std::pair<size_t, Variable> > &indexes, const Variable &val) {
	Variable *cur_var = this;
	for (size_t i = 0; i < indexes.size(); i++) {
		try {
			if (cur_var->type == Type::INTEGER) {
				if (i + 1 < indexes.size())
					throw RuntimeError("Cannot apply multiple [] to Integer for mutable result", indexes[i + 1].first);
				long long index = (long long) indexes[i].second;
				if (index < 0 || index >= (long long)(CHAR_BIT * sizeof(long long)) - 1)
					throw RangeError("Integer", CHAR_BIT * sizeof(long long) - 1, index);
				bool set = (bool) val;
				if (set) cur_var->integer |= (1LL << index);
				else cur_var->integer &= ~(1LL << index);
				return;
			} else if (cur_var->type == Type::STRING) {
				if (i + 1 < indexes.size())
					throw RuntimeError("Cannot apply multiple [] to String for mutable result", indexes[i + 1].first);
				long long index = (long long) indexes[i].second;
				if (index < 0 || ((size_t) index) >= cur_var->str.size()) throw RangeError("String", cur_var->str.size(), index);
				if (((long long) val) < 0 || ((long long) val) > 255) throw RuntimeError("Value out of range for char of String");
				cur_var->str.at(index) = (long long) val;
				return;
			} else if (cur_var->type == Type::ARRAY) {
				long long index = (long long) indexes[i].second;
				if (index < 0 || ((size_t) index) >= cur_var->array.size()) throw RangeError("Array", cur_var->array.size(), index);
				cur_var = &cur_var->array.at(index);
			} else if (cur_var->type == Type::ADVANCED_ARRAY) {
				long long index = (long long) indexes[i].second;
				if (index < 0 || ((size_t) index) >= cur_var->advanced_array.size())
					throw RangeError("Advanced Array", cur_var->advanced_array.size(), index);
				cur_var = &cur_var->advanced_array[index];
			} else if (cur_var->type == Type::SET) {
				if (i + 1 < indexes.size())
					throw RuntimeError("Cannot apply multiple [] to Set for mutable result", indexes[i + 1].first);
				set.remove_index((long long) indexes[i].second);
				set.insert(val);
				return;
			} else throw RuntimeError("No operator [] to "s + type_to_str(type) + " for mutable result", indexes[i].first);
		} catch (RuntimeError &error) {
			if (error.pos == (size_t) -1) error.pos = indexes[i].first;
			throw;
		}
	}
	*cur_var = val;
}
////////


void Variable::output(FILE *stream) const {
	if (type == Type::INTEGER) output_ll(stream, integer);
	else if (type == Type::FLOAT) fprintf(stream, "%.9f", floating);
	else if (type == Type::STRING) fwrite(str.c_str(), 1, str.size(), stream);
	else if (type == Type::ARRAY) {
		for (size_t i = 0; i < array.size(); i++) {
			if (i) fputc(' ', stream);
			array[i].output();
		}
	} else if (type == Type::ADVANCED_ARRAY) advanced_array.output(stream);
	else if (type == Type::SET) set.output(stream);
	else if (type == Type::NONE) fprintf(stream, "(null)");
	else if (type == Type::PTR) fprintf(stream, "Pointer (Internal type)");
	else assert(0);
}
