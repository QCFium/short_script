rwildcard = $(foreach d, $(wildcard $1*), \
            $(filter $(subst *, %, $2), $d) \
			$(call rwildcard, $d/, $2))

COMPILER  = g++
CFLAGS    = -MMD -MP -Wall -Wextra -Winit-self -Wpedantic -O2 -pg
LDFLAGS = -pg
LIBS      =
INCLUDE   = -I./src
TARGET    = ./out
SRCDIR    = ./src
ifeq "$(strip $(SRCDIR))" ""
  SRCDIR  = .
endif
SOURCES   = $(rwildcard $(SRCDIR)/*.cpp)
OBJDIR    = ./obj
ifeq "$(strip $(OBJDIR))" ""
  OBJDIR  = .
endif
OBJECTS = $(patsubst $(SRCDIR)/%.cpp, $(OBJDIR)/%.o, \
          $(call rwildcard, $(SRCDIR), *.cpp))
DEPENDS   = $(OBJECTS:.o=.d)

ifeq ($(OS),Windows_NT)
    PY3 := py -3
else
    PY3 := python3
endif


$(TARGET): $(OBJECTS) $(LIBS)
	@echo "linking..."
	@$(COMPILER) -o $@ $^ $(LDFLAGS)

$(OBJDIR)/%.o: $(SRCDIR)/%.cpp
	@mkdir -p "$(@D)"
	@echo $<
	@$(COMPILER) $(CFLAGS) $(INCLUDE) -o $@ -c $<
	
clean:
	@echo "Cleaning..."
	@rm -f $(OBJECTS) $(DEPENDS) $(TARGET)

test:
	@$(PY3) tests/test.py

-include $(DEPENDS)

