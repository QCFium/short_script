#include <vector>
#include <climits>
#include <cassert>
#include <iostream>
#include <algorithm>
#include "variable.h"
#include "set_string.h"

// exclude longer elements
size_t Set_String::_get_self_size(size_t node) const {
	size_t left = nodes[node].left;
	size_t right = nodes[node].right;
	size_t left_size = 0;
	size_t right_size = 0;
	if (left != (size_t) -1) left_size = nodes[left].get_size(unique);
	if (right != (size_t) -1) right_size = nodes[right].get_size(unique);
	return nodes[node].get_size(unique) - left_size - right_size;
}

// returns how many elements that is lower than str are there
size_t Set_String::_count_lower(const std::string &str) const {
	size_t result = 0;
	size_t cur_node = 0;
	for (size_t i = 0; i < str.size(); i++) { 
		result += _get_self_size(cur_node);
		for (size_t j = CHAR_BIT - 1; j < CHAR_BIT; j--) {
			if (!nodes[cur_node].get_size(unique)) return result;
			size_t left = nodes[cur_node].left;
			size_t right = nodes[cur_node].right;
			if (((unsigned char) str[i]) & 1U << j) {
				if (left != (size_t) -1) result += nodes[left].get_size(unique);
				cur_node = right;
			} else cur_node = left;
			if (cur_node == (size_t) -1) return result;
		}
	}
	return result;
}
size_t Set_String::_index_to_node_index(size_t index) const {
	assert(index < size());
	size_t cur_node = 0;
	// move to the leaf
	while (1) {
		size_t ending_num = _get_self_size(cur_node);
		if (index < ending_num) break; // string end in this node
		else index -= ending_num;
		for (size_t i = 0; i < CHAR_BIT; i++) {
			const Node &node = nodes[cur_node];
			if (node.left == (size_t) -1 && node.right == (size_t) -1)
				assert(!"Internal Error in Set_String::_index_to_node_index");
			if (node.left == (size_t) -1) {
				cur_node = node.right; // no left leaf, go right
			} else if (index >= nodes[node.left].get_size(unique)) { // out of left leaf size, go right
				cur_node = node.right;
				index -= nodes[node.left].get_size(unique);
			} else cur_node = node.left; // inside the left leaf, go left
			assert(cur_node != (size_t) -1);
		}
		if (nodes[cur_node].left == (size_t) -1 && nodes[cur_node].right == (size_t) -1) break;
	}
	return cur_node;
}

Set_String::Set_String(bool is_multi) {
	this->unique = !is_multi;
	nodes.resize(1);
	nodes[0] = Node(-1); // the root
}

void Set_String::set_multi(bool multi) {
	unique = !multi;
	if (!multi && _size != _size_unique) {
		_size = _size_unique;
		for (size_t i = 1; i < nodes.size(); i++)
			nodes[i].subtree_size = nodes[i].subtree_size_unique;
	}
}

size_t Set_String::size() const {
	return unique ? _size_unique : _size;
}

std::string Set_String::operator[] (size_t index) const {
	size_t cur_node = _index_to_node_index(index); // get the leaf
	std::string res;
	while (1) {
		if (cur_node == 0) break;
		unsigned char cur_char = 0;
		for (size_t i = CHAR_BIT - 1; i < CHAR_BIT; i--) {
			size_t parent = nodes[cur_node].parent;
			assert(parent != (size_t) -1);
			if (nodes[parent].right == cur_node) cur_char |= 1U << (CHAR_BIT - 1 - i);
			cur_node = parent;
		}
		res.push_back(cur_char);
	}
	std::reverse(res.begin(), res.end());
	return res;
}

void Set_String::insert(const std::string &str, size_t num) {
	if (unique) num = (bool) num;
	std::vector<size_t> path;
	size_t cur_node = 0;
	for (size_t i = 0; i < str.size(); i++) {
		for (size_t j = CHAR_BIT - 1; j < CHAR_BIT; j--) {
			path.push_back(cur_node);
			if (((unsigned char) str[i]) & 1U << j) {
				// bit is set, move right
				if (nodes[cur_node].right == (size_t) -1) {
					nodes[cur_node].right = nodes.size();
					nodes.push_back(Node(cur_node));
				}
				cur_node = nodes[cur_node].right;
			} else {
				// bit isn't set, move left
				if (nodes[cur_node].left == (size_t) -1) {
					nodes[cur_node].left = nodes.size();
					nodes.push_back(Node(cur_node));
				}
				cur_node = nodes[cur_node].left;
			}
		}
	}
	
	bool is_new = !_get_self_size(cur_node);
	if (!is_new && unique) return;
	
	path.push_back(cur_node);
	for (auto node : path) {
		nodes[node].subtree_size += num;
		if (is_new) nodes[node].subtree_size_unique++;
	}
	_size += num;
	if (is_new) _size_unique++;
}

void Set_String::remove(const std::string &str) {
	size_t cur_node = 0;
	std::vector<size_t> path;
	for (size_t i = 0; i < str.size(); i++) {
		for (size_t j = CHAR_BIT - 1; j < CHAR_BIT; j--) {
			if (!nodes[cur_node].subtree_size) break;
			path.push_back(cur_node);
			if (((unsigned char) str[i]) & 1U << j) { // bit is set, move right
				if (nodes[cur_node].right == (size_t) -1) return; // no str in the set
				cur_node = nodes[cur_node].right;
			} else { // bit isn't set, move left
				if (nodes[cur_node].left == (size_t) -1) return; // no str in the set
				cur_node = nodes[cur_node].left;
			}
		}
	}
	size_t leaf_size = _get_self_size(cur_node);
	if (!leaf_size) return; // there's a node but no content
	size_t remove_num = unique ? leaf_size : 1;
	bool complete_remove = (leaf_size == remove_num);
	
	path.push_back(cur_node);
	for (auto node : path) {
		nodes[node].subtree_size -= remove_num;
		if (complete_remove) nodes[node].subtree_size_unique--;
	}
	_size -= remove_num;
	if (complete_remove) _size_unique--;
}

void Set_String::remove_index(size_t index) {
	assert(index < size());
	size_t leaf = _index_to_node_index(index);
	size_t leaf_size = _get_self_size(leaf);
	if (!leaf_size) return;
	size_t remove_num = unique ? leaf_size : 1;
	bool complete_remove = (leaf_size == remove_num);
	while (1) {
		for (size_t i = 0; i < CHAR_BIT; i++) {
			nodes[leaf].subtree_size -= remove_num;
			if (complete_remove) nodes[leaf].subtree_size_unique--;
			leaf = nodes[leaf].parent;
			assert(leaf != (size_t) -1);
		}
		if (leaf == 0) break;
	}
	_size -= remove_num;
	if (complete_remove) _size_unique--;
}

size_t Set_String::count(const std::string &str) const {
	size_t cur_node = 0;
	for (size_t i = 0; i < str.size(); i++) {
		for (size_t j = CHAR_BIT - 1; j < CHAR_BIT; j--) {
			if (((unsigned char) str[i]) & 1U << j) { // bit is set, move right
				cur_node = nodes[cur_node].right;
			} else { // bit isn't set, move left
				cur_node = nodes[cur_node].left;
			}
			if (cur_node == (size_t) -1) return 0; // no str in the Set
		}
	}
	size_t res = _get_self_size(cur_node);
	if (unique) return (bool) res;
	else return res;
}

size_t Set_String::lower_bound(const std::string &str) const {
	return _count_lower(str);
}
size_t Set_String::upper_bound(const std::string &str) const {
	size_t res = _count_lower(str);
	res += this->count(str);
	return res;
}

void Set_String::get_all(std::vector<Variable> &result) const {
	result.clear();
	for (size_t i = 0; i < size(); i++)
		result.push_back(Variable((*this)[i]));
}
void Set_String::output(FILE *stream) const {
	for (size_t i = 0; i < size(); i++) {
		if (i) fputc(' ', stream);
		std::string str = (*this)[i];
		fwrite(str.c_str(), 1, str.size(), stream);
	}
}
