#include <cassert>
#include "short_script.h"
#include "structure/sort.h"

using compound_func_t = void(Variable &, const std::vector<const Variable *> &, size_t op_pos);

namespace CompoundOps {
	compound_func_t add;
	compound_func_t sub;
	compound_func_t sort_less;
	compound_func_t sort_greater;
	compound_func_t swap;
	compound_func_t update_max;
	compound_func_t update_min;
	compound_func_t to_single_set;
	compound_func_t to_multi_set;
	compound_func_t insert;
	compound_func_t sorted_insert;
}

void ShortScript::run_compound_operation(const Lvalue &target, bool eval) {
	assert(source[parse_head] == '#');
	if (parse_head + 1 >= size) throw Expected("a character after '#'", -1);
	size_t op_pos = parse_head;
	struct CompoundOp {
		compound_func_t *func;
		std::vector<bool> params; // param list, stores if it's lvalue or not(just an expression)
		CompoundOp(compound_func_t func, const std::vector<bool> &params) : func(func), params(params) {}
	};
	static const std::map<char, CompoundOp> operators = {
		{'+', {CompoundOps::add, { false }}},
		{'-', {CompoundOps::sub, { false }}},
		{'<', {CompoundOps::sort_less, {}}},
		{'>', {CompoundOps::sort_greater, {}}},
		{'^', {CompoundOps::swap, { true }}},
		{'M', {CompoundOps::update_max, { false }}},
		{'m', {CompoundOps::update_min, { false }}},
		{'.', {CompoundOps::to_single_set, {}}},
		{':', {CompoundOps::to_multi_set, {}}},
		{'i', {CompoundOps::insert, { false, false }}},
		{'b', {CompoundOps::sorted_insert, { false }}}
	};
	parse_head++;
	char op_ch = source[parse_head];
	if (!operators.count(op_ch)) throw SyntaxError("Undefined compound operator:#"s + op_ch, parse_head);
	const CompoundOp &op = operators.at(op_ch);
	std::vector<const Variable *> args;
	size_t head = parse_head + 1;
	for (size_t i = 0; i < op.params.size(); i++) {
		if (head >= size) throw Expected("argument " + std::to_string(i + 1) + " for compound operator #"s + op_ch, -1);
		if (op.params[i]) { // complete lvalue argument required
			size_t end;
			if (eval) {
				Lvalue *arg;
				end = parser.parse_lvalue(head, &arg);
				if (end != (size_t) -1) args.push_back(&arg->get_complete_lvalue(parser));
			} else end = parser.parse_lvalue(head, nullptr);
			if (end == (size_t) -1)
				throw SyntaxError("Argument " + std::to_string(i + 1) + " of compound operator #"s + op_ch + " must be lvalue", head);
			head = end;
		} else {
			size_t end;
			if (eval) {
				const Variable *arg;
				end = parser.parse_rvalue(head, &arg, false, true, true);
				if (end != head) {
					args.push_back(arg);
				} else assert(0);
			} else end = parser.parse_rvalue(head, (Variable**) nullptr, false, true, true);
			assert(end != head);
			head = end;
		}
	}
	parse_head = head;
	if (eval) {
		try {
			op.func(target.get_complete_lvalue(parser), args, op_pos);
		} catch (RuntimeError &error) {
			if (error.pos_valid && error.pos == (size_t) -1) {
				// dummy applied
				parser.stack_trace.push({"while applying this compound operator", op_pos});
				error.pos_valid = false;
			}
			throw;
		}
	}
}


/* ------------ below is the acutal implementation of each compound operators ------------ */

void CompoundOps::add(Variable &target, const std::vector<const Variable *> &args, size_t op_pos) {
	assert(args.size() == 1);
	if (target.type == Variable::Type::ARRAY) target.array.push_back(*args[0]);
	else if (target.type == Variable::Type::ADVANCED_ARRAY) target.advanced_array.push_back(*args[0]);
	else if (target.type == Variable::Type::SET) target.set.insert(*args[0]);
	else if (target.type == Variable::Type::STRING) target.str.push_back((long long) *args[0]);
	else throw RuntimeError("Undefined compound operator #+ for " + type_to_str(target.type), op_pos);
}
void CompoundOps::sub(Variable &target, const std::vector<const Variable *> &args, size_t op_pos) {
	assert(args.size() == 1);
	if (target.type == Variable::Type::SET) target.set.remove(*args[0]);
	else if (target.type == Variable::Type::ADVANCED_ARRAY) {
		long long index = (long long) *args[0];
		if (index < 0 || ((size_t) index) >= target.advanced_array.size())
			throw RangeError("Advanced Array", target.advanced_array.size(), index);
		target.advanced_array.remove_index(index);
	} else throw RuntimeError("Undefined compound operator #- for " + type_to_str(target.type), op_pos);
}
void CompoundOps::sort_less(Variable &target, const std::vector<const Variable *> &args, size_t op_pos) {
	assert(!args.size());
	if (target.type == Variable::Type::ARRAY) {
		try {
			sort_array(target.array, false);
		} catch (RuntimeError &error) {
			error.set_pos(-1); // dummy
			throw;
		}
	} else if (target.type == Variable::Type::ADVANCED_ARRAY) {
		std::vector<Variable> all;
		target.advanced_array.get_all(all);
		try {
			sort_array(all, false);
		} catch (RuntimeError &error) {
			error.set_pos(-1); // dummy
			throw;
		}
		target = Variable(AdvancedArray(all));
	} else throw RuntimeError("Undefined compound operator #< for " + type_to_str(target.type), op_pos);
}
void CompoundOps::sort_greater(Variable &target, const std::vector<const Variable *> &args, size_t op_pos) {
	assert(!args.size());
	if (target.type == Variable::Type::ARRAY) {
		try {
			sort_array(target.array, true);
		} catch (RuntimeError &error) {
			error.set_pos(-1); // dummy
			throw;
		}
	} else if (target.type == Variable::Type::ADVANCED_ARRAY) {
		std::vector<Variable> all;
		target.advanced_array.get_all(all);
		try {
			sort_array(all, true);
		} catch (RuntimeError &error) {
			error.set_pos(-1); // dummy
			throw;
		}
		target = Variable(AdvancedArray(all));
	} else throw RuntimeError("Undefined compound operator #< for " + type_to_str(target.type), op_pos);
}
void CompoundOps::swap(Variable &target, const std::vector<const Variable *> &args, size_t op_pos) {
	(void) op_pos;
	assert(args.size() == 1);
	std::swap(target, *const_cast<Variable *>(args[0]));
}
void CompoundOps::update_max(Variable &target, const std::vector<const Variable *> &args, size_t op_pos) {
	(void) op_pos;
	assert(args.size() == 1);
	try {
		if (target < *args[0]) target = *args[0];
	} catch (RuntimeError &error) {
		error.set_pos(-1);
		throw;
	}
}
void CompoundOps::update_min(Variable &target, const std::vector<const Variable *> &args, size_t op_pos) {
	(void) op_pos;
	assert(args.size() == 1);
	try {
		if (target > *args[0]) target = *args[0];
	} catch (RuntimeError &error) {
		error.set_pos(-1);
		throw;
	}
}
void CompoundOps::to_single_set(Variable &target, const std::vector<const Variable *> &args, size_t op_pos) {
	assert(!args.size());
	if (target.type == Variable::Type::SET) {
		target.set.set_multi(false);
	} else throw RuntimeError("Undefined compound operator #. for " + type_to_str(target.type), op_pos);
}
void CompoundOps::to_multi_set(Variable &target, const std::vector<const Variable *> &args, size_t op_pos) {
	assert(!args.size());
	if (target.type == Variable::Type::SET) {
		target.set.set_multi(true);
	} else throw RuntimeError("Undefined compound operator #. for " + type_to_str(target.type), op_pos);
}

void CompoundOps::insert(Variable &target, const std::vector<const Variable *> &args, size_t op_pos) {
	assert(args.size() == 2);
	if (target.type == Variable::Type::ADVANCED_ARRAY) {
		long long index = (long long) *args[0];
		if (index < 0 || ((size_t) index) > target.advanced_array.size())
			throw RangeError("Advanced Array", target.advanced_array.size(), index);
		target.advanced_array.insert(index, *args[1]);
	} else throw  RuntimeError("Undefined compound operator #i for " + type_to_str(target.type), op_pos);
}
void CompoundOps::sorted_insert(Variable &target, const std::vector<const Variable *> &args, size_t op_pos) {
	assert(args.size() == 1);
	if (target.type == Variable::Type::ADVANCED_ARRAY) target.advanced_array.sorted_insert(*args[0]);
	else throw  RuntimeError("Undefined compound operator #b for " + type_to_str(target.type), op_pos);
}

