#pragma once
#include <vector>
#include "variable.h"

class ExpressionParser;
struct Lvalue {
	char name = '\0';
	// pair of the position of '[' and evaluated index expression
	std::vector<std::pair<size_t, Variable> > indexes;
	bool auto_assigned = false;
	size_t pos = -1;
	Lvalue(char name, const std::vector<std::pair<size_t, Variable> > &indexes, size_t pos) {
		this->name = name;
		this->indexes = indexes;
		this->pos = pos;
	}
	Lvalue() {} // null
	void assign_new_counter(ExpressionParser &);
	void free(ExpressionParser &);
	void set(ExpressionParser &, const Variable &) const;
	void set(ExpressionParser &, Variable &&) const; // breaks the second argument
	Variable &get_complete_lvalue(ExpressionParser &) const;
};
