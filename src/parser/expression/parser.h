#pragma once
#include <iostream>
#include <stack>
#include <map>
#include <cassert>
#include "variable.h"
#include "parser/operators.h"
#include "lvalue.h"
#include "error/error.h"
#include "indexed_list.h"

extern const std::map<char, Variable::Type> type_of_specifier;

class Token;
class ExpressionParser {
	// Compiled[i] : procedures to be executed when reached at the index i of indexed_list
	struct Procedure {
		enum Type {
			OP, // including array subscription
			CAST, // cast
			EVAL // eval variable or literal
		} type;
		size_t pos = -1; // the position in source, used for error message
		op_func_t *op_func; // for OP
		size_t target; // first operand for OP, target for CAST and EVAL
		size_t target2; // second operand for OP (-1 if it's unary)
		char cast_to; // for CAST
		bool allow_ptr; // for OP and EVAL
		bool lvalue = false; // for EVAL, true if lvalue is required
		Procedure(Type type, size_t pos, size_t target, bool allow_ptr = false) :
			type(type), pos(pos), target(target), allow_ptr(allow_ptr) {}
		Procedure(size_t pos, op_func_t *op_func, size_t target, size_t target2 = -1) :
			type(OP), pos(pos), op_func(op_func), target(target), target2(target2) {}
	};
	struct CompiledToken {
		static const char TYPE_RVALUE = 0;
		static const char TYPE_LVALUE = (char) -1;
		char control = TYPE_RVALUE; // TYPE_RVALUE, TYPE_LVALUE or control char(?:;)
		
		size_t pos;
		Variable var; // set if it's a literal(not variable)
		Lvalue lvalue; // set if it's a lvalue
		size_t static_index = -1; // for constant, index in static_const
		bool broken = false;
		CompiledToken &operator = (const CompiledToken &src) {
			var = src.var;
			lvalue = src.lvalue;
			control = src.control;
			broken = true;
			return *this;
		}
		CompiledToken(char control, size_t pos) : control(control), pos(pos) {}
		CompiledToken(size_t static_index, size_t pos) : pos(pos), static_index(static_index) {}
		CompiledToken(const Lvalue &lvalue, size_t pos) : pos(pos), lvalue(lvalue) {
			control = TYPE_LVALUE;
		}
	};
	std::vector<Variable> static_const;
	
	struct CompiledExpression {
		std::vector<CompiledToken> exps;
		std::vector<std::vector<Procedure> > procedures;
		std::vector<size_t> res_indexes; // position(in exps) of result, paired with procedures
		std::vector<size_t> control_indexes; // position(in exps) of control token(?:;) + end of expression(== exps.size())
		bool procedure_exists;
		size_t end = -1;
		bool is_lvalue;
	};
	std::vector<CompiledExpression> compile_cache[2][2];
	std::vector<bool> compile_cache_available[2][2];
	
	// parse expression functions
	size_t _split_tokens_worker(std::list<Token> &exps, size_t start, bool conditional, bool comparator);
	std::pair<size_t, indexed_list<CompiledToken> & > _split_tokens(size_t start, bool conditional, bool comparator, bool just_try);
	bool _is_lvalue(const indexed_list<Token> &);
	
	// expression compiling functions
	size_t _compile_pure_exp(indexed_list<Token> &exps, size_t start,
		std::vector<Procedure> &, const std::map<size_t, size_t> &squash);
	void _compile_exp(const indexed_list<Token> &_exps, CompiledExpression &res);
	void _cache_compile(size_t start, bool conditional, bool comparator, bool just_try);
	
	// expression evaluation functions
	void _apply_one_procedure(std::vector<CompiledToken> &, const Procedure &, bool lvalue);
	CompiledToken &_eval_compiled_exp(CompiledExpression &compiled, bool lvalue);
	
	// internal token parse functions
	std::pair<size_t, Operator> _get_operator(size_t pos, bool expect_binary);
	size_t _get_unevaled_token(size_t pos, bool expect_binary, Token &res);
	size_t _skip_single_exp(size_t pos);
	
public:
	const std::string &source;
	size_t size;
	Variable *vars; // 52(26 + 26) elements
	bool *var_set; // 52(26 + 26) elements
	std::stack<ErrorInfo> stack_trace;
	
	explicit ExpressionParser (const std::string &source, Variable *vars, bool *var_set)
		: source(source), vars(vars), var_set(var_set) {};

	void init() {
		size = source.size();
		for (size_t i = 0; i < 2; i++)
			for (size_t j = 0; j < 2; j++) {
				compile_cache[i][j].resize(source.size());
				compile_cache_available[i][j].resize(source.size(), false);
			}
	}
	
	// these 2 functions : caution to life time of content of pointer argument res_*,
	// it will be alive until next call to the same position is made
	
	// if failed, throws Expected if !allow_empty otherwise returns pos, **res_var may have Variable::Type::PTR
	size_t parse_rvalue(size_t pos, Variable **res_var, bool allow_empty, bool conditional, bool comparator, bool res_break = true);
	// same as above but **res will never have Variable::Type::PTR
	size_t parse_rvalue(size_t pos, const Variable **res, bool allow_empty, bool conditional, bool comparator) {
		assert(res);
		Variable *tmp;
		size_t end = parse_rvalue(pos, &tmp, allow_empty, conditional, comparator, false);
		if (tmp->type == Variable::Type::PTR) *res = tmp->ptr;
		else *res = tmp;
		return end;
	}
	// returns -1 if failed
	size_t parse_lvalue(size_t pos, Lvalue **res_lvalue);
	
	size_t eval_literal(size_t pos, Variable &res);
	// about variable
	char get_next_counter_name(void);
	void get_input(Variable &) const;
	void set_var(char name, const std::vector<std::pair<size_t, Variable> > &indexes, const Variable &);
	void set_var(char name, const std::vector<std::pair<size_t, Variable> > &indexes, Variable &&); // breaks the last argument
};

struct Token {
	enum Type {
		OP,
		UNEVALED,
		EVALED,
		CONTROL
	} type;
	
	Operator op;
	std::string control;
	size_t _pos;

	Token (const Operator &op) : type(OP), op(op) {}
	Token (const std::string &control, size_t pos = -1) : type(CONTROL), control(control), _pos(pos) {}
	Token (size_t pos) : type(UNEVALED), _pos(pos) {}
	
	inline size_t pos() const {
		if (type == OP) return op.pos;
		else return _pos;
	}
	void output(FILE *stream) const {
		if (type == OP) fprintf(stream, "op:%c %s", op.symbol, op.is_binary ? "binary" : "unary");
		else if (type == CONTROL) fprintf(stream, "control:%s", control.c_str());
		else if (type == UNEVALED) fprintf(stream, "unevaled pos:%d", (int) pos());
		else if (type == EVALED) fprintf(stream, "evaled pos:%d", (int) pos());
		else assert(!"Unexpected type(Internal Error)");
	}
};
