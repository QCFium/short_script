#include <algorithm>
#include "sort.h"
#include "parser/operators.h"

static void sort_int_(std::vector<Variable> &array, bool inverse) {
	std::vector<long long> ints;
	ints.reserve(array.size());
	std::transform(array.begin(), array.end(), std::back_inserter(ints), 
		[] (const Variable &var) { return var.integer; });
	if (inverse) std::sort(ints.begin(), ints.end(), std::greater<long long>());
	else std::sort(ints.begin(), ints.end());
	for (size_t i = 0; i < array.size(); i++) array[i].integer = ints[i];
}

static void sort_str_(std::vector<Variable> &array, bool inverse) {
	if (inverse) {
		std::sort(array.begin(), array.end(), [](const Variable &a, const Variable &b) {
			return a.str > b.str;
		});
	} else {
		std::sort(array.begin(), array.end(), [](const Variable &a, const Variable &b) {
			return a.str < b.str;
		});
	}
}

static void down(std::vector<Variable> &array, size_t n, size_t pos, bool inverse) {
	size_t cur = pos;
	while (1) {
		size_t next = cur;
		size_t left_child = (cur << 1) + 1;
		if (left_child < n) {
			bool update;
			if (inverse) update = array[left_child] < array[cur];
			else update = array[left_child] > array[cur];
			if (update) next = left_child;
		}
		size_t right_child = left_child + 1;
		if (right_child < n) {
			bool update;
			if (inverse) update = array[right_child] < array[next];
			else update = array[right_child] > array[next];
			if (update) next = right_child;
		}
		if (next == cur) break;
		std::swap(array[cur], array[next]);
		cur = next;
	}
}

void sort_array(std::vector<Variable> &array, bool inverse) {
	if (array.size() <= 1) return;
	Variable::Type type = array[0].type;
	bool single_type = true;
	for (size_t i = 1; i < array.size(); i++) {
		if (type != array[i].type) {
			single_type = false;
			break;
		}
	}
	if (single_type && type == Variable::Type::INTEGER) {
		sort_int_(array, inverse);
	} else if (single_type && type == Variable::Type::STRING) {
		sort_str_(array, inverse);
	} else {
		size_t n = array.size();
		// manual heap sort(std::sort does not support throwing errors inside comparator)
		// construct heap
		for (size_t i = (n - 2) / 2; i < n; i--) {
			// down
			down(array, n, i, inverse);
		}
		// sort
		for (size_t i = 0; i + 1 < n; i++) {
			std::swap(array[0], array[n - i - 1]);
			down(array, n - i - 1, 0, inverse);
		}
	}
}

