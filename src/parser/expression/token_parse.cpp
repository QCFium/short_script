#include <cassert>
#include <iostream>
#include <algorithm>
#include "parser.h"

std::pair<size_t, Operator> ExpressionParser::_get_operator(size_t pos, bool expect_binary) {
	bool is_advanced = false;
	if (source[pos] == '$') {
		if (pos + 1 >= size) return {(size_t) -1, {}};
		is_advanced = true;
		pos++;
	}
	auto found = std::find_if(operators.begin(), operators.end(), [&] (const Operator &op) {
		if (!op.is_binary && op.is_advanced) return op.symbol == source[pos] && expect_binary && is_advanced;
		return op.is_binary == expect_binary && op.symbol == source[pos] && op.is_advanced == is_advanced;
	});
	if (found != operators.end()) return {pos + 1, *found};
	return {(size_t) -1, {}}; // null
}

size_t ExpressionParser::_get_unevaled_token(size_t pos, bool expect_binary, Token &res) {
	while (pos < size && isspace(source[pos])) pos++; // skip whitespaces
	if (pos >= size) return -1;
	// first try operator
	auto op_res = _get_operator(pos, expect_binary);
	if (op_res.first != (size_t) -1) {
		Operator op = op_res.second;
		op.pos = pos;
		res = Token(op);
		return op_res.first;
	}
	if (expect_binary) return -1; // literal / variable not expected
	// try normal token(literal / variable)
	size_t end = _skip_single_exp(pos);
	if (end == (size_t)-1) return -1;
	res = Token(pos);
	return end;
}
size_t ExpressionParser::_skip_single_exp(size_t pos) {
	if (std::isdigit(source[pos]) || source[pos] == '.') { // floating or integer literal
		bool is_float = false;
		size_t cur = pos;
		for (; cur < size; cur++) {
			if (source[cur] == '.') {
				if (!is_float) {
					is_float = true;
					continue;
				} else break;
			}
			if (!std::isdigit(source[cur])) break;
		}
		return cur;
	}
	if (std::isalpha(source[pos])) return pos+1;
	if (source[pos] == '_') {
		if (pos + 1 < size && source[pos+1] == '@') {
			if (pos + 2 >= size) throw Expected("type specifier after '@'", -1);
			if (type_of_specifier.count(source[pos+2])) return pos + 3;
			throw SyntaxError("Unknown type specifier:"s + source[pos+2], pos + 2);
		}
		return pos+1;
	}
	if (source[pos] == '\"') {
		size_t cur = pos + 1;
		while (cur < size && source[cur] != '\"') {
			if (source[cur] == '\\') cur += 2;
			else cur++;
		}
		return cur+1; // skip the "
	}
	if (source[pos] == '[' && pos + 1 < size && source[pos + 1] == ']') { // [] is an empty array
		return pos + 2;
	}
	if (source[pos] == '{' && pos + 1 < size && source[pos + 1] == '}') { // {} is an empty set
		return pos + 2;
	}
	return -1; // invalid token
}

size_t ExpressionParser::eval_literal(size_t pos, Variable &res) {
	while (pos < size && isspace(source[pos])) pos++; // skip whitespaces
	assert(pos < size);
	// floating or integer literal
	if (std::isdigit(source[pos]) || source[pos] == '.') {
		bool is_float = false;
		size_t cur = pos;
		for (; cur < size; cur++) {
			if (source[cur] == '.') {
				if (!is_float) {
					is_float = true;
					continue;
				} else break;
			}
			if (!std::isdigit(source[cur])) break;
		}
		// special floating literal '.' => 0.0
		if (cur-pos == 1 && source[pos] == '.') {
			res = Variable(0.0);
			return cur;
		}
		
		if (is_float) {
			char *end;
			double val = std::strtod(&source[pos], &end);
			if (end != &source[cur])
				std::cerr << "Warning : floating literal parsing might failed" << std::endl;
			res = Variable(val);
			return end-&source[0];
		} else {
			char *end;
			long long val = std::strtoll(&source[pos], &end, 10);
			if (end != &source[cur])
				std::cerr << "Warning : integer literal parsing might failed" << std::endl;
			res = Variable(val);
			return end - &source[0];
		}
	}
	if (source[pos] == '\"') {
		size_t cur = pos + 1;
		res = Variable("");
		std::string &str = res.str;
		while (cur < size && source[cur] != '\"') {
			if (source[cur] == '\\') {
				if (cur + 1 < size) {
					cur++;
					if (source[cur] == 'a') str += '\a';
					else if (source[cur] == 'b') str += '\b';
					else if (source[cur] == 'n') str += '\n';
					else if (source[cur] == 'r') str += '\r';
					else if (source[cur] == 't') str += '\t';
					else if (source[cur] == 'f') str += '\f';
					else if (source[cur] == 'v') str += '\v';
					else if (source[cur] == '\'') str += '\'';
					else if (source[cur] == '\"') str += '\"';
					else if (source[cur] == '\\') str += '\\';
					else throw SyntaxError("Unknown escaped char:"s + source[cur], cur);
				} else str += '\\';
			} else str += source[cur];
			cur++;
		}
		return cur+1;
	}
	if (source[pos] == '[') { // [] is an empty array
		assert(pos + 1 < size);
		assert(source[pos + 1] == ']');
		res = Variable(std::vector<Variable>());
		return pos + 2;
	}
	if (source[pos] == '{') { // {} is an empty set
		assert(pos + 1 < size);
		assert(source[pos + 1] == '}');
		res = Variable(Set());
		return pos + 2;
	}
	if (std::isalpha(source[pos]) || source[pos] == '_') return -1; // not a literal but a token
	throw SyntaxError("Invalid literal", pos);
}
