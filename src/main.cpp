#include <fstream>
#include <iostream>
#include <cstring>
#include "short_script.h"

#define SOURCE_LENGTH 0x1000000

static char source[SOURCE_LENGTH];
int main(int argc, char **argv) {
	if (argc < 2) {
		std::cerr << "Error : argument required" << std::endl;
		return -1;
	}
	char *source_path = argv[1];
	std::ifstream source_stream(source_path, std::ios::in | std::ios::binary);
	if (!source_stream) {
		std::cerr << "Error : could not open source file" << std::endl;
		return 1;
	}
	source_stream.read(source, SOURCE_LENGTH-1);
	ShortScript script(source);
	
	for (size_t i = 2; i < (size_t) argc; i++) {
		if (strcmp(argv[i], "-dc") == 0) script.show_compile_result = true;
		else std::cerr << "Warning : " << i << "th command-line argument does not do anything" << std::endl;
	}
	
	bool res = script.run();
	if (!res) return 1;
	// script.debug();
	
	return 0;
}
