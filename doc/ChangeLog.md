# v0.0 : Initial release  
  The execution speed is currently extremely slow.  
  Note that this is a pre-release and there can be breaking changes after this release.
  
# v0.1
  - Fixed 3 bugs

# v0.2
  - Execution accelerated(approx 2x)
  - Fixed \\c and \\b not working outside loops
  - Fixed wrong size of Set
  - Added operator * between Strings and Arrays for repeating/reversing
  - Added operator / for splitting String or Array with an element
  - Added operator <,>,= between Arrays and Strings for lexicographical comparison
  - Added operator % for counting element in Array, String or Set

  #### [Breaking Changes]
  ##### Comma operator changes
  ```
  1,2,"abc"
  ```
  before : Array{Array{1,2}, "abc"}  
  after : Array{1,2,"abc"}  
  
# v0.3
  - General execution accelerated(approx 2x)
  - Fixed evaluating both block of conditional expression by mistake
  - Added operator |^& for bit and Set operation
  - Added operator - for Set
  - Added compound operator \#< and \#\> for sorting an array(O(NlogN))
  - Supporting cast from Integer Set to generating Set with integer in range \[0,n\)
  - Integer input accelerated about 10x
  
# v0.4
  - General execution accelerated(approx 5x)
  - Integer/String output accelerated
  - Fixed lvalue handling bugs
  - Fixed behavior of division between negative integers(thanks ibuki2003)
  - Added operator $l and $u for lower/upper_bound of Set/sorted Array
  - Added compound operator #^ to swap two complete lvalues
  - Added compound operator #M and #m for chmax/chmin
  - Added unary operator ~ for bit inversion

# v0.5
  - Accelerated Set of Integer by using AVL Tree instead of Binary Trie  
  - Added a new datatype 'AdvancedArray'  
     Its type specifier is 'A' and it likes Array but it has more powerful functions.  
     'N' refers the number of elements in AdvancedArray.  
     - Almost all operations on Array is also supported on AdvancedArray
     - The only disadvantage from Array is that random access(both read/write) and push_back is slowed down from O(1) to O(logN)
     - In exchange for that, you can insert/delete an element to anywhere in AdvancedArray in O(logN) time.
     - AdvancedArray can be used as std::map, here is the method  
        Make AdvancedArray of 2 elements-Array, the 2 elements will be the key and the value.  
        We will keep AdvancedArray sorted(Arrays are compared by their first elements firstly, so they will be sorted by the key).
        - insertion  
           Make an Array with 2 elements which are the key and the value.
           Do a binary search on an AdvancedArray and insert by compound operator #b(it takes O(logN) time).
        - access  
           Make an Array with 2 elements which are the key and the minimum of the value type.
           Call lower_bound on AdvancedArray to get index of the first element with a key that is not less than key to search.
           You can reference the AdvancedArray with the index got above
           If the element has the same key as the searching key, we found it.
           Otherwise, an element with the searching key does not exist.
        - deletion  
           Do the same stuff as 'access' to get the index.
           If the element with the searching key does not exist, do nothing.
           Otherwise we can remove that element by applying compound operator #-(also O(logN) time).
     - new operators only for AdvancedArray  
        - compound operator #i  
           ```a#ixy```  
		   Insert y into a(AdvancedArray) at position of x(must be in [0, N]).  
		   x means just before x-th element(0-indexed).
		   x=N means the back.  
		   Takes O(logN) time.
		- compound operator #- for AdvancedArray  
		   ```a#-i```  
		   Remove the i-th element of a(AdvancedArray)  
		   Takes O(logN) time.
		- compound operator #b  
		   ```a#by```  
		   a must be sorted.  
		   Insert y into a at where a remains sorted.  
		   Takes O(logN) time.
  - Added suffix unary operator $d to decode a char
  - Added operator $c to count a element in Set/Array/AdvancedArray/String  
    It has the same function as %  
	**IMPORTANT** : operator % for this function is deprecated and will be removed in a future release.  
  - Add support for Arrat/AdvancedArray/String/Integer(bit) subscription like ```a[1,3]```  
    This code takes sub-array starting at position 1(0-indexed) and its length will be 3.
  - A token in source can be separated by one or more whitespaces.(This is not under language specifications)  
  - Evaluation sequence in an expression is now unspecified(example below)
  #### [Breaking Changes]
  ##### input evaluation sequence
  code
  ```
  _%_
  ```
  input
  ```
  2 1
  ```
  before : ```0```  
  after :```0``` or ```1```(unspecified)  
