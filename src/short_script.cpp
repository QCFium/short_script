#include <string>
#include <cassert>
#include "short_script.h"
#include "structure/set.h"
#include "error/error.h"
#include "color/colors.h"

void ShortScript::init_line_handler() {
	// squash line break and whitespaces
	std::string new_source;
	size_t line_cnt = 0;
	size_t pos_in_line_cur = 0;
	for (size_t i = 0; i < size; i++) {
		if (source[i] == '\n') {
			line_cnt++;
			pos_in_line_cur = 0;
		} else if (source[i] == '\r') {
			line_cnt++;
			pos_in_line_cur = 0;
			if (i + 1 < size && source[i + 1] == '\n') i++;
		} else {
			new_source.push_back(source[i]);
			line_num.push_back(line_cnt);
			pos_in_line.push_back(pos_in_line_cur++);
		}
	}
	source = new_source;
	size = new_source.size();
}

#define VIEW_FIRST 20
#define VIEW_SECOND 20
void ShortScript::output_source(size_t pos) {
	if (pos >= size) {
		assert(size);
		pos = size;
	}
	// output first half
	size_t line_start;
	if (pos >= size) line_start = (pos - 1) - pos_in_line[pos - 1];
	else line_start = pos - pos_in_line[pos];
	size_t start = std::max((long long) line_start, ((long long) pos) - VIEW_FIRST);
	std::cerr << source.substr(start, pos - start);
	
	// output the middle and latter half
	if (pos >= size) {
		std::cerr << BACKGROUND_RED << " " << COLOR_RESET;
	} else {
		std::cerr << CHAR_RED << source[pos] << COLOR_RESET;
		size_t line_end = pos;
		const size_t cur_line_num = line_num[pos];
		for (; line_end < size; line_end++)
			if (line_num[line_end] != cur_line_num) break;
		line_end--;
		size_t len = std::min(size - 1 - pos, std::min(line_end - pos, (size_t) VIEW_SECOND));
		std::cerr << source.substr(pos + 1, len);
	}
	std::cerr << std::endl;
}
