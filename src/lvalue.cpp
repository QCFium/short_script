#include <cassert>
#include "lvalue.h"
#include "error/error.h"
#include "parser/expression/parser.h"
#include "utils.h"

void Lvalue::assign_new_counter(ExpressionParser &parser) {
	this->name = parser.get_next_counter_name();
	if (!this->name) throw RuntimeError("Could not assign counter variable");
	indexes = {};
	auto_assigned = true;
}
void Lvalue::free(ExpressionParser &parser) {
	if (auto_assigned) parser.var_set[char_to_index(name)] = false;
}
void Lvalue::set(ExpressionParser &parser, const Variable &x) const {
	try {
		parser.set_var(name, indexes, x);
	} catch (RuntimeError &error) {
		error.set_pos(pos);
		throw;
	}
}
void Lvalue::set(ExpressionParser &parser, Variable &&x) const {
	try {
		parser.set_var(name, indexes, std::forward<Variable>(x));
	} catch (RuntimeError &error) {
		error.set_pos(pos);
		throw;
	}
}
Variable &Lvalue::get_complete_lvalue(ExpressionParser &parser) const {
	size_t index = char_to_index(name);
	if (!parser.var_set[index]) throw RuntimeError("Use of complete lvalue of an undefined variable:"s + name);
	Variable *res = parser.vars + index;
	for (size_t i = 0; i < indexes.size(); i++) {
		if (res->type == Variable::Type::ARRAY) {
			long long index = (long long) indexes[i].second;
			try {
				if (index < 0 || ((size_t) index) >= res->array.size())
					throw RangeError("Array", res->array.size(), index);
				res = &res->array.at(index);
			} catch (RuntimeError &error) {
				error.set_pos(indexes[i].first);
				throw;
			}
		} else if (res->type == Variable::Type::ADVANCED_ARRAY) {
			long long index = (long long) indexes[i].second;
			try {
				if (index < 0 || ((size_t) index) >= res->advanced_array.size())
					throw RangeError("Advanced Array", res->advanced_array.size(), index);
				res = &res->advanced_array[index];
			} catch (RuntimeError &error) {
				error.set_pos(indexes[i].first);
				throw;
			}
		} else throw RuntimeError("No operator [] for " + type_to_str(res->type) + " with complete lvalue result");
	}
	return *res;
}
