#include <cassert>
#include <algorithm>
#include <unordered_map>
#include <iostream>
#include "parser.h"
#include "utils.h"

// hash support for iterator
template<class T>
struct list_iterator_hash {
    size_t operator()(const typename std::list<T>::iterator &i) const {
        return std::hash<T*>()(&*i);
    }
};

struct ExpBlock {
	enum ExpBlockType {
		TYPE_PARENTHESIS,
		TYPE_ARRAY_INDEX,
		TYPE_IF
	};
	ExpBlockType type;
	indexed_list<Token>::iterator pos;
	bool eval;
	indexed_list<Token>::iterator start; // only for TYPE_IF
	// constructor
	ExpBlock(ExpBlockType type, indexed_list<Token>::iterator pos, bool eval = false) :
		type(type), pos(pos), eval(eval) {}
	ExpBlock(indexed_list<Token>::iterator pos, decltype(pos) start, bool eval) :
		type(TYPE_IF), pos(pos), eval(eval), start(start) {}
		
	void insert_close_token(indexed_list<Token> &exps, size_t pos) const {	
		if (type == TYPE_PARENTHESIS) exps.push_back(Token(")", pos));
		else if (type == TYPE_ARRAY_INDEX) exps.push_back(Token("]", pos));
		else if (type == TYPE_IF) exps.push_back(Token(";", pos));
		else assert(!"Unexpected value in ExpBlockType(Internal Error)");
	}
	// only for TYPE_IF
	bool is_main_block() const {
		assert(type == TYPE_IF);
		return pos->type == Token::CONTROL && pos->control == "?";
	}
};

size_t ExpressionParser::_split_tokens_worker(std::list<Token> &exps, size_t start, bool conditional, bool comparator) {
	for (auto c : ")]?:") if (source[start] == c) return start;
	if (source[start] == '[' && (start + 1 >= size || source[start + 1] != ']')) return start;
	struct ParseBlock {
		enum Type {
			BLOCK_PARENTHESIS,
			BLOCK_ARRAY_INDEX,
			BLOCK_IF_MAIN,
			BLOCK_IF_ELSE
		} type;
		std::list<Token>::iterator pos; // used for inserting '(' implicitly when '@' appeared
		ParseBlock (Type type, std::list<Token>::iterator pos) : type(type), pos(pos) {}
		std::string get_closer() {
			if (type == BLOCK_PARENTHESIS) return ")";
			else if (type == BLOCK_ARRAY_INDEX) return "]";
			else if (type == BLOCK_IF_ELSE) return ";";
			assert(!"Unexpected BLOCK_IF_MAIN");
		}
	};
	std::vector<ParseBlock> blocks;
	
	size_t cur = start;
	bool expect_var = true;
	bool is_top_block_empty = true;
	while (cur < size) {
		while (cur < size) {
			if (!comparator && (source[cur] == '<' || source[cur] == '>')) {
				// if that's not inside () or [], stop parsing
				if (std::all_of(blocks.begin(), blocks.end(), [](const ParseBlock &block) {
					return block.type != ParseBlock::BLOCK_PARENTHESIS && block.type != ParseBlock::BLOCK_ARRAY_INDEX;
				})) break;
			}
			if (source[cur] == ':') {
				if (expect_var) {
					if (exps.size() && exps.back().type == Token::CONTROL && exps.back().control == "?")
						throw Expected("primary expression", cur); // empty primary expression
					else if (is_top_block_empty) throw Expected("expression", cur); // closing empty block
					else throw Expected("unary expression", cur);
				}
				bool close_success = false;
				while (blocks.size()) {
					auto &top = blocks.back();
					if (top.type == ParseBlock::BLOCK_IF_MAIN) {
						// still not reached 'else' block, switch it to 'else' block
						exps.push_back(Token(":", cur));
						top = ParseBlock(ParseBlock::BLOCK_IF_ELSE, std::prev(exps.end()));
						close_success = true;
						expect_var = true;
						is_top_block_empty = true;
						cur++;
						break;
					}
					exps.push_back(Token(top.get_closer(), cur));
					blocks.pop_back();
				}
				if (close_success) continue;
				break; // that ':' should be closing 'if' *statement*
			}
			if (source[cur] == '(') {
				if (!expect_var) break;
				exps.push_back(Token("(", cur));
				blocks.push_back(ParseBlock(ParseBlock::BLOCK_PARENTHESIS, std::prev(exps.end())));
				expect_var = true; // parse expression inside the parenthesis, so expect var
				is_top_block_empty = true; // it's empty yet
				cur++;
				continue;
			}
			if (source[cur] == '[' && cur + 1 < size && source[cur + 1] != ']') {
				if (expect_var) {
					if (exps.size() && !is_top_block_empty) throw Expected("unary expression", cur);
					else throw Expected("expression", cur);
				}
				exps.push_back(Token("[", cur));
				blocks.push_back(ParseBlock(ParseBlock::BLOCK_ARRAY_INDEX, std::prev(exps.end())));
				expect_var = true; // parse expression inside the parenthesis, so expect var
				is_top_block_empty = true; // it's empty yet
				cur++;
				continue;
			}
			if (source[cur] == ')' || source[cur] == ']') {
				if (expect_var) {
					if (exps.size() && exps.back().type == Token::CONTROL) {
						if (exps.back().control == ":")
							throw Expected("secondary expression", cur); // empty secondary expression
						else if (exps.back().control == "?")
							throw Expected("primary expression", cur); // empty primary expression
					}
					else if (is_top_block_empty) throw Expected("expression", cur); // closing empty block
					else throw Expected("unary expression", cur);
				}
				if (!blocks.size()) break;
				auto &top = blocks.back();
				// in these cases, the top block is closed successfully
				if (top.type == ParseBlock::BLOCK_PARENTHESIS && source[cur] == ')') cur++;
				if (top.type == ParseBlock::BLOCK_ARRAY_INDEX && source[cur] == ']') cur++;
				
				if (top.type == ParseBlock::BLOCK_IF_MAIN)
					throw SyntaxError("Closing if expression without else block implicitly",  cur);
				
				expect_var = false; // any block is parsed as a value
				exps.push_back(Token(top.get_closer(), cur));
				blocks.pop_back();
				continue;
			}
			if (source[cur] == '@') {
				if (cur + 1 >= size) throw Expected("type specifier after '@'", -1);
				if (expect_var) break;
				if (type_of_specifier.count(source[cur+1])) {
					// insert () to handle low priority of the cast operator
					auto insert_itr = exps.begin();
					if (blocks.size()) insert_itr = std::next(blocks.back().pos);
					exps.insert(insert_itr, Token("("));
					exps.push_back(Token(")"));
					
					exps.push_back(Token(source.substr(cur, 2), cur));
					cur += 2;
					continue;
				} else throw SyntaxError("Unknown type specifier:"s + source[cur+1], cur+1);
			}
			if (source[cur] == '?') { // 'if' expression
				if (!conditional && !blocks.size()) break;
				if (expect_var) break;
				exps.push_back(Token("?", cur));
				blocks.push_back(ParseBlock(ParseBlock::BLOCK_IF_MAIN, std::prev(exps.end())));
				expect_var = true;
				is_top_block_empty = true;
				cur++;
				continue;
			}
			Token next_token(-1);
			size_t token_end = _get_unevaled_token(cur, !expect_var, next_token); // expect binary op iff var is not expected
			if (token_end == (size_t) -1) break; // end of expression(including '?',':' and other ctrl symbols)
			if (next_token.type == Token::OP) {
				if (!next_token.op.is_binary && next_token.op.is_advanced) expect_var = false; // suffix unary operator
				else expect_var = true;
			} else expect_var = false;
			is_top_block_empty = false;
			exps.push_back(next_token);
			cur = token_end;
		}
		if (!exps.size()) break;
		if (exps.size() && exps.back().type == Token::CONTROL && exps.back().control == "?")
			throw Expected("primary expression", cur); // empty primary expression
		if (expect_var) {
			if (is_top_block_empty) throw Expected("expression", cur);
			else throw Expected("unary expression", cur);
		}
		bool switch_else_success = false;
		// expression finished, find 'if' expression not reached its else block yet
		while (blocks.size()) {
			auto &top = blocks.back();
			if (top.type == ParseBlock::BLOCK_IF_MAIN) {
				top = ParseBlock(ParseBlock::BLOCK_IF_ELSE, std::prev(exps.end()));
				exps.push_back(Token(":", cur));
				expect_var = true;
				is_top_block_empty = true;
				switch_else_success = true;
				break;
			}
			expect_var = false;
			exps.push_back(Token(top.get_closer(), cur));
			blocks.pop_back();
		}
		if (!switch_else_success) break;
	}
	if (!exps.size()) return start;
	if (expect_var) throw Expected("unary expression", cur);
	// close unclosed blocks
	if (exps.back().type == Token::CONTROL && exps.back().control == ":")
		throw Expected("secondary expression", cur); // empty secondary expression
	while (blocks.size()) {
		auto &top = blocks.back();
		if (top.type == ParseBlock::BLOCK_IF_MAIN)
			throw SyntaxError("Closing if expression without else block implicitly at the end of expression", cur);
		exps.push_back(Token(top.get_closer(), cur));
		blocks.pop_back();
	}
	return cur;
}

bool ExpressionParser::_is_lvalue(const indexed_list<Token> &_exps) {
	// in this function, Token::EVALED means rvalue, Token::UNEVALED means lvalue
	std::list<Token> exps = (std::list<Token>) _exps;
	assert(exps.size());
	// decide rvalue or lvalue for each token
	for (auto &i : exps) if (i.type == Token::UNEVALED) {
		if (std::isalpha(source[i.pos()]) || source[i.pos()] == '_') i.type = Token::UNEVALED; // lvalue
		else i.type = Token::EVALED; // rvalue
	}
	using itr_t = std::list<Token>::iterator;
	std::stack<itr_t> blocks;
	for (auto cur = exps.begin(); cur != exps.end(); ) {
		if (cur->type == Token::CONTROL) {
			if (cur->control == "(" || cur->control == "[") blocks.push(cur), cur++;
			else if (cur->control == ")") {
				if (std::prev(cur, 2) != blocks.top()) { // if the content has 2 or more tokens...
					// it's a rvalue !! (hacky)
					exps.erase(blocks.top(), cur);
					cur->type = Token::EVALED;
					cur++;
				} else {
					// depends if the content is lvalue or not
					exps.erase(blocks.top());
					cur = exps.erase(cur);
				}
				blocks.pop();
			} else if (cur->control == "]") {
				// depends if the target(parent array) is lvalue or not
				cur = exps.erase(blocks.top(), std::next(cur));
				blocks.pop();
			} else if (cur->control == "?" || cur->control == ":") blocks.push(cur), cur++;
			else if (cur->control == ";") {
				itr_t colon_pos = blocks.top();
				blocks.pop();
				itr_t question_pos = blocks.top();
				blocks.pop();
				itr_t condition_start = blocks.size() ? std::next(blocks.top()) : exps.begin();
				if (std::prev(cur, 2) == colon_pos && std::prev(cur)->type == Token::UNEVALED &&
					std::prev(colon_pos, 2) == question_pos && std::prev(colon_pos)->type == Token::UNEVALED) {
					// both lvalue, result will be lvalue
					cur->type = Token::UNEVALED;
				} else {
					cur->type = Token::EVALED;
				}
				exps.erase(condition_start, cur);
				cur++;
			} else if (cur->control[0] == '@') { cur++; } // cast
			else assert(!"Unexpected control token");
		} else cur++;
	}
	assert(!blocks.size());
	return exps.size() == 1 && exps.begin()->type == Token::UNEVALED;
}

// 'compile' expressions without (), a?b:c or a[b] into operator procedure
// argument 'start_pos' is the index in indexed_list
// returns the end of the expression
size_t ExpressionParser::_compile_pure_exp(indexed_list<Token> &exps, size_t start_,
	std::vector<Procedure> &procedure, const std::map<size_t, size_t> &squash) {
	// compied_exp holds only '?', ':', ';' and the actual value(not operators or other control token)
	// squash is used to convert from index in exps to that in compiled tokens
	
	auto start = exps.index(start_); // iterator pointing the start of the expression
	size_t source_pos = exps.begin()->pos(); // position of start of the expression in the source
	
	auto end = start;
	for (; end != exps.end() && end->type != Token::CONTROL; end++);
	
	// apply suffix operators
	static std::vector<Operator> unary_ops;
	for (auto cur = std::prev(end); cur != std::prev(start); cur--) {
		if (cur->type == Token::OP) {
			if (!cur->op.is_binary && cur->op.is_advanced) {
				unary_ops.push_back(cur->op);
				cur = exps.erase(cur);
			}
		}
		if (cur->type == Token::UNEVALED && unary_ops.size()) {
			// target eval
			cur->type = Token::EVALED;
			procedure.push_back(Procedure(Procedure::EVAL, cur->pos(), squash.at(cur.pos), false));
		}
		if (cur->type == Token::EVALED && unary_ops.size()) {
			for (auto op = unary_ops.rbegin(); op != unary_ops.rend(); op++) {
				Procedure cur_procedure(op->pos, op->function, squash.at(cur.pos));
				cur_procedure.allow_ptr = false; // suffix unary operators don't take pointer-argument
				procedure.push_back(cur_procedure);
			}
			unary_ops.clear();
		}
	}
	assert(!unary_ops.size());
	
	// apply prefix operators
	for (auto cur = start; cur != end; ) {
		if (cur->type == Token::OP && !cur->op.is_binary) {
			assert(!cur->op.is_advanced); // those should be already erased above
			unary_ops.push_back(cur->op);
			if (cur == start) cur = start = exps.erase(cur);
			else cur = exps.erase(cur);
			continue;
		}
		if (cur->type == Token::UNEVALED && unary_ops.size()) {
			// target eval
			bool allow_ptr = false;
			for (auto op_func : ptr_supported) if (op_func == unary_ops.back().function) allow_ptr = true;
			cur->type = Token::EVALED;
			procedure.push_back(Procedure(Procedure::EVAL, cur->pos(), squash.at(cur.pos), allow_ptr));
		}
		if (cur->type == Token::EVALED && unary_ops.size()) {
			for (auto op = unary_ops.rbegin(); op != unary_ops.rend(); op++) {
				bool allow_ptr = false;
				for (auto op_func : ptr_supported) if (op_func == op->function) allow_ptr = true;
				Procedure cur_procedure(op->pos, op->function, squash.at(cur.pos));
				cur_procedure.allow_ptr = allow_ptr; // suffix unary operators don't take pointer-argument
				procedure.push_back(cur_procedure);
			}
			unary_ops.clear();
		}
		cur++;
	}
	assert(!unary_ops.size());
	
	for (auto op_group : op_groups) {
		auto start_prev = std::prev(start); // save the previous iterator of start(iterators are looping at the end)
		for (auto cur = start; cur != end; ) {
			if (cur->type == Token::OP) {
				assert(cur->op.is_binary);
				// binary operator handling
				if (std::all_of(op_group.begin(), op_group.end(), [&cur] (const Operator &op) {
					return op != cur->op;
				})) {
					cur++;
					continue;
				}
				bool allow_ptr = false;
				for (auto op_func : ptr_supported) if (op_func == cur->op.function) allow_ptr = true;
				
				// first operand eval handling
				auto target = std::prev(cur);
				if (target->type == Token::UNEVALED) {
					target->type = Token::EVALED;
					procedure.push_back(Procedure(Procedure::EVAL, target->pos(), squash.at(target.pos), allow_ptr));
				}
				assert(target->type == Token::EVALED);
				
				// second operand eval handling
				auto target2 = std::next(cur);
				if (target2->type == Token::UNEVALED) { // not constant(variable or '_')
					target2->type = Token::EVALED;
					procedure.push_back(Procedure(Procedure::EVAL, target2->pos(), squash.at(target2.pos), false));
				}
				assert(target2->type == Token::EVALED);
				
				// operator applying procedure
				Procedure cur_procedure(cur->pos(), cur->op.function, squash.at(target.pos), squash.at(target2.pos));
				cur_procedure.allow_ptr = allow_ptr;
				procedure.push_back(cur_procedure); // intentionally not cur->pos
				
				exps.erase(std::next(cur)); // erase second operand
				std::prev(cur)->type = Token::EVALED; // make it evaled(dummy)
				cur = exps.erase(cur); // erase the operator
			} else cur++;
		}
		start = std::next(start_prev); // restore the backup as 'start' maybe invalidated 
	}
	assert(!unary_ops.size());
	if (exps.size() != 1) Fatal("exps.size() not 1 in _compile_pure_exp : " + std::to_string(exps.size()), source_pos);
	assert(std::next(start) == exps.end() || std::next(start)->type == Token::CONTROL);
	return std::next(start).pos; // intentionally not ->pos
}


// 'compile' an expression into procedure
// sets except res.end and res.is_lvalue
void ExpressionParser::_compile_exp(const indexed_list<Token> &_exps, CompiledExpression &res) {
	res.procedures.resize(_exps.size() + 1);
	res.res_indexes.resize(_exps.size() + 1);
	indexed_list<Token> exps = _exps;
	std::vector<Procedure> procedure;
	indexed_list<CompiledToken> compiled_exps;
	std::map<size_t, size_t> squash;
	
	// first, store all things we need to compiled_exps, 
	for (auto itr = exps.begin(); itr != exps.end(); itr++) {
		if (itr->type == Token::UNEVALED) {
			squash[itr.pos] = compiled_exps.size();
			Variable constant;
			if (eval_literal(itr->pos(), constant) != (size_t) -1) {
				itr->type = Token::EVALED;
				compiled_exps.push_back(CompiledToken(static_const.size(), itr->pos()));
				compiled_exps.back().var = constant;
				static_const.push_back(constant);
			} else compiled_exps.push_back(CompiledToken((size_t) -1, itr->pos()));
		} else if (itr->type == Token::CONTROL) {
			if (itr->control == "?" || itr->control == ":" || itr->control == ";") {
				squash[itr.pos] = compiled_exps.size();
				compiled_exps.push_back(CompiledToken(itr->control[0], itr->pos()));
			}
		}
	}
	
	std::vector<bool> is_lvalue_token(compiled_exps.size()); // stores squashed index of tokens that should be evaled as lvalue
	// parse from the end, see if each element is parsed as lvalue
	// iterator pointing the block end(as we're parsing from the end, we'll reach there first)
	{
		std::stack<indexed_list<Token>::iterator> blocks;
		size_t last_lvalue_disable = -1; // the level of last block which causes lvalue disable
		for (auto itr = std::prev(exps.end()); itr != exps.end(); itr--) {
			if (itr->type == Token::CONTROL) {
				if (itr->control == ")" || itr->control == ";") blocks.push(itr);
				else if (itr->control == "]") {
					if (last_lvalue_disable == (size_t) -1) last_lvalue_disable = blocks.size();
					blocks.push(itr);
				} 
				if (itr->control == ":" || itr->control == "?" || itr->control == "(" || itr->control == "[") {
					while (blocks.size()) {
						if (blocks.top()->control == "?") blocks.pop();
						else break;
					}
				}
				if (itr->control == ":") {
					blocks.top() = itr;
				} else if (itr->control == "?") {
					blocks.top() = itr;
					if (blocks.size() <= last_lvalue_disable) last_lvalue_disable = -1;
					if (last_lvalue_disable == (size_t) -1) last_lvalue_disable = blocks.size() - 1;
				} else if (itr->control == "(" || itr->control == "[") {
					blocks.pop();
					if (blocks.size() <= last_lvalue_disable) last_lvalue_disable = -1;
				}
			} else if (itr->type == Token::UNEVALED) {
				is_lvalue_token[squash.at(itr.pos)] = (last_lvalue_disable == (size_t) -1);
			}
		}
	}
	
	std::stack<ExpBlock> blocks;
	while (1) {
		auto exp_start = exps.begin();
		if (blocks.size()) {
			exp_start = std::next(blocks.top().pos);
		}
		
		auto end = exp_start;
		while (end != exps.end()) {
			if (end->type == Token::CONTROL) break;
			end++;
		}
		// only parse 'expression' on block end(or expression end)
		if (end == exps.end() || end->control == ";" || end->control == ":" ||
			end->control == ")" || end->control == "]" || end->control == "?") {
			size_t end_index = _compile_pure_exp(exps, exp_start.pos, procedure, squash);
			assert(end_index == end.pos);
		}
		if (end == exps.end()) break;
		assert(end->type == Token::CONTROL);
		if (end->control == ")") {
			assert(end != exps.begin());
			assert(std::prev(end) != exps.begin());
			assert(std::prev(end)->type == Token::EVALED || std::prev(end)->type == Token::UNEVALED);
			assert(std::prev(end, 2)->type == Token::CONTROL);
			assert(std::prev(end, 2)->control == "(");
			
			exps.erase(std::prev(end, 2)); // erase '('
			exps.erase(end); // erase ')'
			
			assert(blocks.size());
			assert(blocks.top().type == ExpBlock::TYPE_PARENTHESIS);
			blocks.pop();
		} else if (end->control == "]") {
			assert(end != exps.begin());
			assert(std::prev(end) != exps.begin());
			assert(std::prev(end, 2) != exps.begin()); // '[' is should not be the start(target exists)
			
			// parent eval handling
			auto parent_itr = std::prev(end, 3);
			if (parent_itr->type == Token::UNEVALED) {
				parent_itr->type = Token::EVALED;
				procedure.push_back(Procedure(Procedure::EVAL, parent_itr->pos(), squash.at(parent_itr.pos), true));
			}
			assert(parent_itr->type == Token::EVALED);
			
			// index eval handling
			auto index_itr = std::prev(end);
			if (index_itr->type == Token::UNEVALED) { // not constant(variable or '_')
				index_itr->type = Token::EVALED;
				procedure.push_back(Procedure(Procedure::EVAL, index_itr->pos(), squash.at(index_itr.pos), false));
			}
			assert(index_itr->type == Token::EVALED);
			
			Procedure array_procedure(std::prev(end, 2)->pos(), &Operators::array_get,
				squash.at(std::prev(end, 3).pos), squash.at(std::prev(end).pos));
			array_procedure.allow_ptr = true; // Operators::array_get should be in ptr_supported
			procedure.push_back(array_procedure);
			
			exps.erase(std::prev(end, 2)); // '['
			exps.erase(std::prev(end)); // index
			exps.erase(end); // ']'
			
			assert(blocks.size());
			assert(blocks.top().type == ExpBlock::TYPE_ARRAY_INDEX);
			blocks.pop();
		} else if (end->control == ":") {
			assert(end != exps.begin());
			assert(blocks.size());
			auto &top = blocks.top();
			assert(top.type == ExpBlock::TYPE_IF);
			assert(top.is_main_block());
			
			auto main_itr = std::prev(end);
			if (!procedure.size() && main_itr->type == Token::UNEVALED) {// need to eval the main block
				main_itr->type = Token::EVALED;
				procedure.push_back(Procedure(Procedure::EVAL, main_itr->pos(), squash.at(main_itr.pos), true));
			}
			res.procedures[squash.at(end.pos)] = procedure;
			res.res_indexes[squash.at(end.pos)] = squash.at(main_itr.pos);
			procedure.clear();
			
			top.pos = end;
		} else if (end->control == ";") {
			assert(end != exps.begin());
			assert(blocks.size());
			auto &top = blocks.top();
			assert(top.type == ExpBlock::TYPE_IF);
			assert(!top.is_main_block());
			
			auto else_itr = std::prev(end);
			if (!procedure.size() && else_itr->type == Token::UNEVALED) {// need to eval the else block
				else_itr->type = Token::EVALED;
				procedure.push_back(Procedure(Procedure::EVAL, else_itr->pos(), squash.at(else_itr.pos), true));
			}
			res.procedures[squash.at(end.pos)] = procedure;
			res.res_indexes[squash.at(end.pos)] = squash.at(else_itr.pos);
			procedure.clear();
			
			exps.erase(top.start, std::next(end)); // erase from '?' to ';'
			blocks.pop();
		} else if (end->control == "?") {
			assert(end != exps.begin());
			blocks.push(ExpBlock(end, end, false)); // false is a dummy
			
			auto cond_itr = std::prev(end);
			if (!procedure.size() && cond_itr->type == Token::UNEVALED) { // need to eval the condition
				cond_itr->type = Token::EVALED;
				procedure.push_back(Procedure(Procedure::EVAL, cond_itr->pos(), squash.at(cond_itr.pos), false));
			}
			// save procedure since it's branching here
			res.procedures[squash.at(end.pos)] = procedure;
			res.res_indexes[squash.at(end.pos)] = squash.at(cond_itr.pos);
			procedure.clear();
		} else if (end->control == "(") {
			blocks.push(ExpBlock(ExpBlock::TYPE_PARENTHESIS, end, false)); // false is a dummy
		} else if (end->control == "[") {
			blocks.push(ExpBlock(ExpBlock::TYPE_ARRAY_INDEX, end, false)); // false is a dummy
		} else if (end->control[0] == '@') {
			assert(end != exps.begin());
			auto target_itr = std::prev(end);
			if (!procedure.size() && target_itr->type == Token::UNEVALED) { // need to eval the target
				target_itr->type = Token::EVALED;
				procedure.push_back(Procedure(Procedure::EVAL, target_itr->pos(), squash.at(target_itr.pos), true));
			}
			Procedure cast_procedure(Procedure::CAST, end->pos(), squash.at(target_itr.pos));
			cast_procedure.cast_to = end->control[1];
			procedure.push_back(cast_procedure);
			exps.erase(end);
		} else assert(!"Unexpected control token");
	}
	assert(exps.size() == 1);
	if (exps.begin()->type == Token::UNEVALED) {
		exps.begin()->type = Token::EVALED;
		procedure.push_back(Procedure(Procedure::EVAL, exps.begin()->pos(), squash.at(exps.begin().pos), false));
	}
	res.procedures[compiled_exps.size()] = procedure;
	res.res_indexes[compiled_exps.size()] = squash.at(exps.begin().pos); // procedure at the end of the expression
	
	res.procedure_exists = false;
	for (auto &i : res.procedures) {
		if (i.size()) res.procedure_exists = true;
		for (auto &one_proc : i) {
			if (one_proc.type == Procedure::EVAL && is_lvalue_token[one_proc.target])
				one_proc.lvalue = true;
		}
	}
	
	/*
	std::cerr << "compiled_exp debug : " << std::endl;
	for (auto &i : compiled_exps) {
		std::cerr << "  token pos:" << i.pos;
		if (i.control == CompiledToken::TYPE_RVALUE) std::cerr << " rvalue";
		else if (i.control == CompiledToken::TYPE_LVALUE) std::cerr << " lvalue";
		else std::cerr << " control:" << i.control;
		std::cerr << std::endl;
	}
	std::cerr << "procedure debug : " << std::endl;
	for (size_t i = 0; i <= _exps.size(); i++) {
		if (res.procedures[i].size()) {
			std::cerr << "  " << i << " : " << std::endl;
			for (auto j : res.procedures[i]) {
				std::cerr << "    source:" << j.pos << "  target:" << j.target << "  ";
				if (j.type == Procedure::BINARY_OP) std::cerr << "binary op";
				else if (j.type == Procedure::UNARY_OP) std::cerr << "unary op";
				else if (j.type == Procedure::EVAL) std::cerr << "eval";
				else if (j.type == Procedure::EVAL_LVALUE) std::cerr << "eval lvalue";
				else if (j.type == Procedure::CAST) std::cerr << "cast:" << j.cast_to;
				std::cerr << std::endl;
			}
		}
	}*/
	/****************************************************/
	/****************************************************/
	/****************************************************/
	/****************************************************/
	for (size_t i = 0; i < compiled_exps.size(); i++) {
		const CompiledToken &cur_token = *compiled_exps.index(i);
		res.exps.push_back(cur_token);
		if (cur_token.control != CompiledToken::TYPE_LVALUE && cur_token.control != CompiledToken::TYPE_RVALUE)
			res.control_indexes.push_back(i);
	}
	res.control_indexes.push_back(res.exps.size()); // add end of expression because we should apply procedure there too
}

void ExpressionParser::_apply_one_procedure(std::vector<CompiledToken> &exps, const Procedure &procedure, bool lvalue) {
	CompiledToken &target = exps[procedure.target];
	if (procedure.type == Procedure::OP) {
		if (procedure.op_func == &Operators::array_get && lvalue && target.control == CompiledToken::TYPE_LVALUE) {
			const CompiledToken &target2 = exps[procedure.target2];
			// lvalue array subscription
			const Variable &target2_var = (target2.var.type == Variable::Type::PTR) ? *target2.var.ptr : target2.var;
			target.lvalue.indexes.emplace_back(target.pos, target2_var);
		} else {
			assert(target.control == CompiledToken::TYPE_RVALUE);
			static const Variable dummy(0LL);
			const Variable *second_operand;
			if (procedure.target2 != (size_t) -1) {
				assert(exps[procedure.target2].control == CompiledToken::TYPE_RVALUE);
				if (exps[procedure.target2].var.type == Variable::Type::PTR) second_operand = exps[procedure.target2].var.ptr;
				else second_operand = &exps[procedure.target2].var;
			} else second_operand = &dummy;
			try {
				// the result will be stored into target->var
				if (!procedure.allow_ptr && target.var.type == Variable::Type::PTR)
					target.var = *target.var.ptr;
				target.broken = true;
				procedure.op_func(target.var, *second_operand);
			} catch (RuntimeError &error) {
				// add information of the position of the operator
				if (!error.pos_valid) error.set_pos(procedure.pos);
				else {
					error.pos_valid = false;
					stack_trace.push({"while applying this operator", procedure.pos});
				}
				throw; // should be catched in ShortScript::run()
			}
		}
	} else if (procedure.type == Procedure::CAST) {
		assert(target.control == CompiledToken::TYPE_RVALUE);
		assert(type_of_specifier.count(procedure.cast_to));
		target.broken = true;
		try {
			if (target.var.type == Variable::Type::PTR)
				target.var.ptr->cast(type_of_specifier.at(procedure.cast_to), target.var);
			else target.var.cast(type_of_specifier.at(procedure.cast_to), target.var);
		} catch (RuntimeError &error) {
			if (!error.pos_valid) error.set_pos(procedure.pos);
			else {
				error.pos_valid = false;
				stack_trace.push({"while applying this operator", procedure.pos});
			}
			throw;
		}
	} else if (procedure.type == Procedure::EVAL) {
		// eval variable or '_'
		if (lvalue && procedure.lvalue) {
			target = CompiledToken(Lvalue(source[procedure.pos], {}, procedure.pos), procedure.pos);
		} else {
			size_t pos = procedure.pos;
			if (source[pos] == '_') {
				if (pos + 1 < size && source[pos + 1] == '@') {
					if (pos + 2 >= size) throw Expected("type specifier after '@'", -1);
					assert(type_of_specifier.count(source[pos+2]));
					target.var.type = type_of_specifier.at(source[pos+2]);
				} else target.var.type = Variable::Type::INTEGER;
				get_input(target.var);
			} else {
				size_t index = char_to_index(source[pos]);
				if (!var_set[index]) throw RuntimeError("Undefined variable referenced : "s + source[pos]);
				if (procedure.allow_ptr) target.var = Variable(vars + index);
				else target.var = vars[index];
			}
		}
	} else throw Fatal("Unexpected type of Procedure in _apply_one_procedure", exps.begin()->pos);
}

auto ExpressionParser::_eval_compiled_exp(CompiledExpression &compiled, bool lvalue) -> CompiledToken & {
	struct Branch {
		size_t pos; // position of '?' or ':'(if reached the 'else' block)
		size_t start; // position of '?'
		bool eval;
		Branch(size_t pos, bool cond) :
			pos(pos), start(pos), eval(cond) {}
	};
	static std::vector<Branch> blocks;
	std::vector<CompiledToken> &exps = compiled.exps;
	size_t exp_size = exps.size();
	assert(exp_size);
	
	// fix broken constant
	for (auto &i : exps) {
		if (i.broken && i.static_index != (size_t) -1) {
			i.broken = false;
			i.var = static_const[i.static_index];
		}
	}
	
	for (size_t control_pos : compiled.control_indexes) {
		bool eval = true;
		// get the end of non-branching expression
		if (blocks.size()) eval = blocks.back().eval;
		
		if (eval) {
			for (const Procedure &procedure : compiled.procedures[control_pos])
				_apply_one_procedure(exps, procedure, lvalue);
		}
		if (control_pos == exp_size) {
			assert(!blocks.size());
			return exps[compiled.res_indexes[exp_size]];
		}
		if (exps[control_pos].control == ':') {
			assert(control_pos > 0);
			assert(blocks.size());
			Branch &top = blocks.back();
			top.pos = control_pos;
			top.eval = !top.eval;
		} else if (exps[control_pos].control == ';') {
			assert(control_pos > 0);
			assert(blocks.size());
			Branch &top = blocks.back();
			bool main_evaled = !top.eval; // whether the main block was evaled or the else block was
			bool if_evaled = (blocks.size() > 1) ? blocks[blocks.size() - 2].eval : true; // whether the if expression itself was evaled
			if (if_evaled) {
				size_t cond_content_index = compiled.res_indexes[top.start];
				if (main_evaled) { // else blocks was evaled(condition is false)
					size_t main_content_index = compiled.res_indexes[top.pos];
					assert(exps[main_content_index].control == CompiledToken::TYPE_RVALUE ||
						   exps[main_content_index].control == CompiledToken::TYPE_LVALUE);
					
					exps[cond_content_index] = exps[main_content_index];
				} else { // main block was evaled(condition is true)
					size_t else_content_index = compiled.res_indexes[control_pos];
					assert(exps[else_content_index].control == CompiledToken::TYPE_RVALUE ||
						   exps[else_content_index].control == CompiledToken::TYPE_LVALUE);
					
					exps[cond_content_index] = exps[else_content_index]; // replace condition expression with the result
				}
			}
			blocks.pop_back();
		} else if (exps[control_pos].control == '?') {
			assert(control_pos > 0);
			bool cond;
			if (eval) {
				CompiledToken &cond_token = exps[compiled.res_indexes[control_pos]];
				assert(cond_token.control == CompiledToken::TYPE_RVALUE);
				cond = (bool) (cond_token.var.type == Variable::Type::PTR ? *cond_token.var.ptr : cond_token.var);
			} else cond = false;
			blocks.push_back(Branch(control_pos, cond));
		} else assert(!"Unexpected control token");
	}
	abort(); // should be unreachable
}

void ExpressionParser::_cache_compile(size_t start, bool conditional, bool comparator, bool just_try) {
	compile_cache_available[conditional][comparator][start] = true;
	CompiledExpression &cur_cache = compile_cache[conditional][comparator][start];
	std::list<Token> exps;
	size_t end;
	try {
		end = _split_tokens_worker(exps, start, conditional, comparator);
	} catch (SyntaxError &error) {
		if (just_try) {
			end = -1;
			exps = {};
		} else throw;
	}
	// fail handling
	if ((start == end) || (end == (size_t) -1) || !exps.size()) {
		if (just_try) {
			cur_cache = {};
			return;
		} else throw Expected("expression", start);
	}
	indexed_list<Token> exps_indexed(exps);
	
	_compile_exp(exps_indexed, cur_cache);
	cur_cache.end = end;
	cur_cache.is_lvalue = _is_lvalue(exps_indexed);
}

size_t ExpressionParser::parse_lvalue(size_t pos, Lvalue **res_lvalue) {
	bool eval = (bool) res_lvalue;
	if (!compile_cache_available[true][true][pos]) _cache_compile(pos, true, true, true);
	CompiledExpression &cur = compile_cache[true][true].at(pos);
	
	// check if that's lvalue syntax
	if (!cur.is_lvalue) return -1;
	
	if (!eval) return cur.end; // if !eval, return the end pos of lvalue
	
	CompiledToken &res = _eval_compiled_exp(cur, true);
	assert(res.control == CompiledToken::TYPE_LVALUE);
	*res_lvalue = &res.lvalue;
	return cur.end;
}

size_t ExpressionParser::parse_rvalue(size_t pos, Variable **res_var, bool allow_empty, bool conditional, bool comparator, bool res_break) {
	bool eval = (bool) res_var; // eval if result pointer is specified
	if (allow_empty) assert(!eval);
	if (!compile_cache_available[conditional][comparator][pos]) _cache_compile(pos, conditional, comparator, allow_empty);
	CompiledExpression &cur = compile_cache[conditional][comparator].at(pos);
	if (cur.end == (size_t) -1) {
		assert(allow_empty);
		return pos; // nothing read
	}
	if (!eval) return cur.end;
	if (!cur.procedure_exists) {
		CompiledToken &res = *cur.exps.begin();
		if (res.broken && res.static_index != (size_t) -1) {
			res.broken = false;
			res.var = static_const[res.static_index];
		}
		*res_var = &res.var;
		if (res_break) res.broken = true;
		return cur.end;
	}
	CompiledToken &res = _eval_compiled_exp(cur, false);
	assert(res.control == CompiledToken::TYPE_RVALUE);
	*res_var = &res.var;
	if (res_break) res.broken = true;
	return cur.end;
}
