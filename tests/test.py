import sys
import glob
import time
import concurrent.futures
import os
import subprocess
import time

class Color:
    BLACK     = '\033[30m'
    RED       = '\033[31m'
    GREEN     = '\033[32m'
    YELLOW    = '\033[33m'
    BLUE      = '\033[34m'
    PURPLE    = '\033[35m'
    CYAN      = '\033[36m'
    WHITE     = '\033[37m'
    END       = '\033[0m'
    BOLD      = '\038[1m'
    UNDERLINE = '\033[4m'
    INVISIBLE = '\033[08m'
    REVERCE   = '\033[07m'


def get_subprocess_flag():
	if sys.platform.startswith("win"):
		return 0x8000000 #win32con.CREATE_NO_WINDOW?
	else:
		return 0

def test_one_group(group_dir):
	results = {
				"group_name":group_dir,
				"tests":[]
			}
	for test_dir in sorted(glob.glob(group_dir + '/*')) : # for each test
		if os.path.isfile(test_dir): continue
		cur_test_result = {
			"name":os.path.split(test_dir)[1],
			"testdata":[]
		}
		for out_name in sorted(glob.glob(test_dir + '/*.out')): # for each testdata
			testdata_path, ext = os.path.splitext(out_name)
			in_name = testdata_path + '.in'
			with open(out_name, 'rb') as out_file:
				cur_result = ""
				if os.path.isfile(in_name):
					# check input
					with open(in_name, 'rb') as in_file:
						proc = subprocess.Popen(['../out', test_dir + '/main.ssc'],
							stdin=in_file, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
				else:
					proc = subprocess.Popen(['../out', test_dir + '/main.ssc'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
				
				try:
					out = proc.communicate(timeout=2)[0]
				except subprocess.TimeoutExpired:
					proc.kill()
					cur_result = "TLE"
				else:
					if proc.returncode != 0:
						cur_result = "RE"
					else: cur_result = "AC" if (out == out_file.read()) else "WA"
			
			testdata_name = os.path.split(testdata_path)[1]
			cur_test_result["testdata"].append((testdata_name, cur_result))
			
		results["tests"].append(cur_test_result)
	
	return results
				
			

def main():
	if sys.platform.startswith("win"):
		# Don't display the Windows GPF dialog if the invoked program dies.
		# See comp.os.ms-windows.programmer.win32
		# How to suppress crash notification dialog?, Jan 14,2004 -
		# Raymond Chen's response [1]

		import ctypes
		SEM_NOGPFAULTERRORBOX = 0x0002 # From MSDN
		ctypes.windll.kernel32.SetErrorMode(SEM_NOGPFAULTERRORBOX)
		
	print("Testing...")
	start = time.time()
		
	executor = concurrent.futures.ProcessPoolExecutor(max_workers=4)
	
	tasks = []
	os.chdir(os.path.dirname(os.path.abspath(__file__)))
	cur_dir = os.path.abspath(os.path.dirname(__file__))
	
	if sys.platform.startswith("win"):
		exec_name = "../out.exe"
	else :
		exec_name = "../out"
	
	if not os.path.isfile(exec_name) :
		print(Color.RED + "Compiled binary not found" + Color.END)
		print(Color.RED + "Type 'make' to build binary" + Color.END)
		quit(1)
		
	for i in glob.glob("*"): # for each test group
		if os.path.isfile(i): continue
		tasks.append(executor.submit(test_one_group, i))
	
	# get results
	results = []
	for i in concurrent.futures.as_completed(tasks):
		results.append(i.result())
	
	results.sort(key=lambda group:group["group_name"])
	
	failed_groups = 0
	# print results
	for result_group in results :
		failed_dirs = 0
		failed_cases = 0
		
		failed_test_strs = []
		for result in result_group["tests"] :
			failed_num = 0
			for case in result["testdata"] :
				if case[1] != "AC": failed_num += 1
			
			failed_cases += failed_num
			if failed_num :
				failed_dirs += 1
				failed_test_strs.append("  [" + Color.RED + "Failed Test" + Color.END + "]  " + result["name"])
			
			for case in result["testdata"] :
				if case[1] == "TLE" :
					failed_test_strs.append("    [" + Color.RED + "Failed(Time Limit Exceeded)" + Color.END + "]  " + case[0])
				elif case[1] == "WA" :
					failed_test_strs.append("    [" + Color.RED + "Failed(Wrong Answer)" + Color.END + "]  " + case[0])
				elif case[1] == "RE" :
					failed_test_strs.append("    [" + Color.PURPLE + "Failed(Crashed)" + Color.END + "]  " + case[0])
		
		if failed_dirs :
			failed_groups += 1
			print("[" + Color.RED + "Failed Test Group" + Color.END + "]  " + result_group["group_name"])
			for message in failed_test_strs :
				print(message)
		else :
			print("[{}Passed Test Group(All {} test(s)){}]  {}".format
				(Color.GREEN, len(result_group["tests"]), Color.END, result_group["group_name"]))
		
					
	if failed_groups :
		print("")
		print(Color.RED + "Test FAILED in " + str(failed_groups) + " group(s)" + Color.END)
	else :
		end = time.time()
		print("")
		print(Color.GREEN + "All tests Passed in {:.3}s".format(end - start) + Color.END)
	
	executor.shutdown()


if __name__ == '__main__':
	main()
