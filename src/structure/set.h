#pragma once
#include <vector>
#include <cstdio>
#include "set/set_integer.h"
#include "set/set_string.h"

// wrapper of Set_Integer and Set_String
class Set {
	Set_Integer *integer_set = nullptr;
	Set_String *string_set = nullptr;
public:
	Set() {};
	Set(const Set &set) { *this = set; };
	Set(Set &&set) { *this = std::forward<Set>(set); }
	explicit Set(bool multi) { is_multi = multi; }
	explicit Set(int n) { integer_set = new Set_Integer(n); } // [0, n)
	Set & operator = (const Set &set);
	Set & operator = (Set &&set);
	~Set();
	
	bool is_multi = false;
	
	size_t size() const;
	void set_multi(bool); // set if it's multiset
	Variable operator[] (long long index) const;
	Set &operator |= (const Set &);
	Set &operator &= (const Set &);
	Set &operator ^= (const Set &);
	Set &operator -= (const Set &);
	void insert(Variable n);
	void remove(Variable n);
	void remove_index(long long index);
	size_t count(Variable n) const;
	size_t lower_bound(const Variable &) const;
	size_t upper_bound(const Variable &) const;
	void get_all(std::vector<Variable> &result) const;
	void output(FILE *stream = stdout) const;
};
