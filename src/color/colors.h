#pragma once

#define CHAR_BLACK  "\033[30m"
#define CHAR_RED    "\033[31m"
#define CHAR_GREEN  "\033[32m"
#define CHAR_YELLOW "\033[33m"
#define CHAR_BLUE   "\033[34m"
#define CHAR_PURPLE "\033[35m"
#define CHAR_CYAN   "\033[36m"
#define CHAR_WHITE  "\033[37m"

#define BACKGROUND_BLACK  "\033[40m"
#define BACKGROUND_RED    "\033[41m"
#define BACKGROUND_GREEN  "\033[42m"
#define BACKGROUND_YELLOW "\033[43m"
#define BACKGROUND_BLUE   "\033[44m"
#define BACKGROUND_PURPLE "\033[45m"
#define BACKGROUND_CYAN   "\033[46m"
#define BACKGROUND_WHITE  "\033[47m"

#define COLOR_RESET "\033[0m"
