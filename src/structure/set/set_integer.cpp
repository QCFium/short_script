#include <cassert>
#include <vector>
#include <iostream>
#include <algorithm>
#include "set_integer.h"
#include "variable.h"
#include "parser/operators.h"
#include "utils.h"

Set_Integer::Set_Integer(bool is_multi) {
	this->is_multi = is_multi;
	nodes.push_back(Node()); // this dummy node will only have the left child
}

Set_Integer::Set_Integer(const std::vector<long long> &array) : Set_Integer(false) {
	_size = array.size();
	if (array.size()) {
		size_t root = _init_copy(&array[0], &array[0] + array.size(), 0);
		nodes[0].left = root;
	}
}
// returns the index of root node constructed
size_t Set_Integer::_init_copy(const long long *start, const long long *end, size_t parent) { // construct tree with elements in [start, end)
	size_t size = end - start;
	assert(size);
	const long long *mid = start + (size >> 1);
	
	// root
	size_t cur_node = nodes.size();
	nodes.push_back(Node(*mid, parent));
	
	// left child
	if (mid != start) { // left construction
		size_t left = _init_copy(start, mid, cur_node);
		nodes[cur_node].left = left;
	}
	if (mid + 1 != end) { // right construction
		size_t right = _init_copy(mid + 1, end, cur_node);
		nodes[cur_node].right = right;
	}
	update_node_info(cur_node);
	return cur_node;
}

Set_Integer::Set_Integer(int n) : Set_Integer(false) {
	if (n < 0) n = 0;
	_size = n;
	if (n) {
		size_t child = _continuous_fill(0, n, 0);
		nodes[0].left = child;
	}
}
size_t Set_Integer::_continuous_fill(int start, int end, size_t parent) {
	assert(end - start);
	int mid = start + (end - start) / 2;
	size_t cur_node = nodes.size();
	nodes.push_back(Node(mid, parent));
	if (mid != start) {
		size_t left = _continuous_fill(start, mid, cur_node);
		nodes[cur_node].left = left;
	}
	if (mid + 1 != end) {
		size_t right = _continuous_fill(mid + 1, end, cur_node);
		nodes[cur_node].right = right;
	}
	return cur_node;
}

void Set_Integer::set_multi(bool is_multi) {
	if (this->is_multi && !is_multi && _size) { // discard multiple element
		std::vector<long long> all;
		_get_all_native(all, nodes[0].left);
		size_t head = 0;
		for (size_t i = 1; i < all.size(); i++) if (all[head] != all[i]) all[++head] = all[i];
		all.resize(head + 1);
		*this = Set_Integer(all);
	}
	this->is_multi = is_multi;
}

void Set_Integer::balance(size_t node, bool is_remove) {
	size_t cur = node;
	while (nodes[cur].parent) {
		size_t next = nodes[cur].parent;
		size_t prev_height = nodes[next].subtree_height;
		if ((nodes[next].left == cur) != is_remove) {
			// left increased
			if (height_sub(next) == 2) {
				if (height_sub(nodes[next].left) >= 0) next = rotate_r(next);
				else next = rotate_lr(next);
			} else update_node_info(next);
		} else {
			// right increased
			if (height_sub(next) == -2) {
				if (height_sub(nodes[next].right) <= 0) next = rotate_l(next);
				else next = rotate_rl(next);
			} else update_node_info(next);
		}
		if (prev_height == nodes[next].subtree_height) break; // height unchanged, further fix is not required
		cur = next;
	}
}
void Set_Integer::insert(long long val) {
	bool moved_right = false;
	size_t parent = 0;
	size_t cur_node = nodes[parent].left;
	std::vector<size_t> path;
	while (cur_node) { // move down the tree
		path.push_back(cur_node);
		parent = cur_node;
		if (nodes[cur_node].val > val) { // move left
			moved_right = false;
			cur_node = nodes[cur_node].left;
		} else if (is_multi || nodes[cur_node].val < val) { // move right
			moved_right = true;
			cur_node = nodes[cur_node].right;
		} else return; // already exists
	}
	for (auto i : path) nodes[i].subtree_size++;
	if (moved_right) nodes[parent].right = nodes.size();
	else nodes[parent].left = nodes.size();
	nodes.push_back(Node(val, parent));
	balance(nodes.size() - 1, false);
	_size++;
}

void Set_Integer::_remove_node(size_t node) {
	size_t left = nodes[node].left;
	size_t right = nodes[node].right;
	if (!left) {
		replace_subtree(node, right);
		balance(right, true);
	} else {
		size_t nearest_left = left;
		while (nodes[nearest_left].right) {
			nodes[nearest_left].subtree_size--;
			nearest_left = nodes[nearest_left].right;
		}
		nodes[node].val = nodes[nearest_left].val;
		replace_subtree(nearest_left, nodes[nearest_left].left);
		balance(nodes[nearest_left].left, true);
	}
}
void Set_Integer::remove_index(size_t pos) {
	assert(pos < _size);
	size_t cur_node = nodes[0].left;
	while (cur_node) { // move down the tree
		nodes[cur_node].subtree_size--;
		size_t left = nodes[cur_node].left;
		size_t left_size = nodes[left].subtree_size;
		if (left_size > pos) cur_node = left;
		else if (left_size < pos) {
			pos -= left_size + 1;
			cur_node = nodes[cur_node].right;
		} else { // remove cur_node
			_remove_node(cur_node);
			_size--;
			return;
		}
	}
	assert(0);
}
void Set_Integer::remove(long long val) {
	size_t cur_node = nodes[0].left;
	std::vector<size_t> path;
	while (cur_node) {
		path.push_back(cur_node);
		if (nodes[cur_node].val > val) cur_node = nodes[cur_node].left;
		else if (nodes[cur_node].val < val) cur_node = nodes[cur_node].right;
		else {
			for (auto i : path) nodes[i].subtree_size--;
			_remove_node(cur_node);
			_size--;
			return;
		}
	}
}

// these two have exactly the same code
long long &Set_Integer::operator [] (size_t index) {
	assert(index < _size);
	size_t cur_node = nodes[0].left;
	while (cur_node) {
		size_t left = nodes[cur_node].left;
		size_t left_size = nodes[left].subtree_size;
		if (left_size > index) cur_node = left;
		else if (left_size < index) {
			index -= left_size + 1;
			cur_node = nodes[cur_node].right;
		} else return nodes[cur_node].val;
	}
	assert(0); // unreachable
	while(1);
}
long long Set_Integer::operator [] (size_t index) const {
	assert(index < _size);
	size_t cur_node = nodes[0].left;
	while (cur_node) {
		size_t left = nodes[cur_node].left;
		size_t left_size = nodes[left].subtree_size;
		if (left_size > index) cur_node = left;
		else if (left_size < index) {
			index -= left_size + 1;
			cur_node = nodes[cur_node].right;
		} else return nodes[cur_node].val;
	}
	assert(0); // unreachable
	while(1);
}

void Set_Integer::_get_all(std::vector<Variable> &all, size_t node) const {
	if (nodes[node].left) _get_all(all, nodes[node].left);
	all.push_back(Variable(nodes[node].val));
	if (nodes[node].right) _get_all(all, nodes[node].right);
}
void Set_Integer::_get_all_native(std::vector<long long> &all, size_t node) const {
	if (nodes[node].left) _get_all_native(all, nodes[node].left);
	all.push_back(nodes[node].val);
	if (nodes[node].right) _get_all_native(all, nodes[node].right);
}
void Set_Integer::get_all(std::vector<Variable> &all) const {
	if (!_size) return;
	_get_all(all, nodes[0].left);
}

void Set_Integer::_output(FILE *stream, size_t node, bool rightest) const {
	if (nodes[node].left) _output(stream, nodes[node].left, false);
	output_ll(stream, nodes[node].val);
	if (nodes[node].right || !rightest) fputc(' ', stream);
	if (nodes[node].right) _output(stream, nodes[node].right, rightest);
}
void Set_Integer::output(FILE *stream) const {
	if (!_size) return;
	_output(stream, nodes[0].left, true);
}

size_t Set_Integer::_binary_search(long long val, bool upper_bound) const {
	size_t res = _size;
	size_t cur_node = nodes[0].left;
	if (!cur_node) return 0;
	size_t cur_index = nodes[nodes[cur_node].left].subtree_size;
	while (cur_node) {
		bool too_left = upper_bound ? !(nodes[cur_node].val > val) :
										(nodes[cur_node].val < val);
		if (too_left) { // move right
			cur_node = nodes[cur_node].right;
			cur_index++;
			if (cur_node) cur_index += nodes[nodes[cur_node].left].subtree_size;
		} else {
			res = cur_index;
			cur_node = nodes[cur_node].left;
			cur_index--;
			if (cur_node) cur_index -= nodes[nodes[cur_node].right].subtree_size;
		}
	}
	return res;
}

void Set_Integer::clear() {
	if (nodes.size() > 1) nodes.erase(nodes.begin() + 1, nodes.end());
	_size = 0;
}

void Set_Integer::debug(size_t node, size_t level) const {
	for (size_t i = 0; i < level; i++) std::cerr << " ";
	if (level && !node) {
		std::cerr << "none" << std::endl;
		return;
	}
	std::cerr << node << ": size:" << nodes[node].subtree_size << " height:" << nodes[node].subtree_height << " val:" << nodes[node].val;
	std::cerr << " parent:" << nodes[node].parent << std::endl;
	debug(nodes[node].left, level + 1);
	debug(nodes[node].right, level + 1);
}
