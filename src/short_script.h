#pragma once
#include <iostream>
#include <string>
#include <map>
#include <vector>
#include "variable.h"
#include "error/error.h"
#include "parser/expression/parser.h"
#include "utils.h"

#define DOUBLE_INF std::numeric_limits<double>::infinity()
#define LL_INF     std::numeric_limits<long long>::max()

class ShortScript {
private:
	struct Block { // internally used for script execution
		enum Type {
			TYPE_FOR,
			TYPE_FOR_ITR
		} type;
		size_t index; // index in std::vector<CompiledStatement>
		Lvalue counter;
		Variable finish;
		bool open;
		bool down;
		
		std::vector<Variable> list;
		size_t itr_pos; // pos in list points
		
		Block(size_t index, const Lvalue &counter, const Variable &finish, bool finish_open, bool down) :
			type(TYPE_FOR), index(index), counter(counter), finish(finish), open(finish_open), down(down) {}
		Block(size_t index, const Lvalue &counter, std::vector<Variable> &&list) :
			type(TYPE_FOR_ITR), index(index), counter(counter), list(list), itr_pos(0) {}
	};
	
	// internally used for script compiling
	struct ParseBlock {
		enum Type {
			TYPE_IF,
			TYPE_FOR
		} type;
		size_t index; // index in std::vector<CompiledStatement>, points the start of the block
		bool is_main; // only for TYPE_IF
		std::vector<size_t> continues;
		std::vector<size_t> breaks;
		ParseBlock(Type type, size_t index = -1, bool is_main = true) :
			type(type), index(index), is_main(is_main) {}
	};
	struct CompiledStatement {
		enum Type {
			TYPE_FOR,
			TYPE_FOR_ITR,
			TYPE_IF,
			TYPE_END,
			TYPE_JUMP,
			TYPE_COMPOUND,
			TYPE_ASSIGN,
			TYPE_OUTPUT,
			TYPE_SPECIAL
		} type;
		size_t start; // pos(in source file) of the start of the statement
		size_t target; // start of 'else' block for TYPE_IF, statement to jump for TYPE_JUMP, end of loop for TYPE_FOR*
		size_t counter_pos; // only for TYPE_FOR*, -1 if unspecified
		size_t for_start; // pos(in source) of start exp for TYPE_FOR, parent for TYPE_FOR_ITR
		size_t for_finish; // pos(in source) of finish exp for TYPE_FOR
		bool for_down;
		bool for_open;
		CompiledStatement(Type type, size_t start, size_t target, size_t counter_pos = -1) :
			type(type), start(start), target(target), counter_pos(counter_pos) {}
		CompiledStatement(Type type, size_t start, size_t counter_pos, size_t for_start, size_t for_finish, bool for_down, bool for_open) :
			type(type), start(start), target(-1), counter_pos(counter_pos), for_start(for_start), for_finish(for_finish),
			for_down(for_down), for_open(for_open) {}
		void output(FILE *stream = stdout) const {
			if (type == TYPE_FOR) fprintf(stream, "for ");
			else if (type == TYPE_FOR_ITR) fprintf(stream, "for itr ");
			else if (type == TYPE_IF) fprintf(stream, "if ");
			else if (type == TYPE_END) fprintf(stream, "end for ");
			else if (type == TYPE_JUMP) fprintf(stream, "jump ");
			else if (type == TYPE_COMPOUND) fprintf(stream, "compound ");
			else if (type == TYPE_ASSIGN) fprintf(stream, "assign ");
			else if (type == TYPE_OUTPUT) fprintf(stream, "output ");
			else if (type == TYPE_SPECIAL) fprintf(stream, "special ");
			fprintf(stream, " start:%zu  target:%zu\n", start, target);
		}
	};
	std::vector<CompiledStatement> compiled_statements;
	
	void init_line_handler(void); // defined in short_script.cpp

	// defined in parser/compound_operator.cpp
	void run_compound_operation(const Lvalue &target, bool eval); 
	
	// defined in parser/script_parse.cpp
	void compile_loop(std::vector<CompiledStatement> &res, std::vector<ParseBlock> &blocks);
	void compile_statement(std::vector<CompiledStatement> &res);
	void show_compiled_visual(const std::vector<CompiledStatement> &) const;
	bool run_statements(const std::vector<CompiledStatement> &);
	
public:
	std::string source;
	size_t size;
	Variable vars[52];
	bool var_set[52];
	ExpressionParser parser = ExpressionParser(source, vars, var_set);
	std::vector<size_t> line_num;
	std::vector<size_t> pos_in_line;
	
	size_t parse_head;
	
	// option flags
	bool show_compile_result = false;
	
	explicit ShortScript(const std::string &source) {
		this->source = source;
		size = source.size();
		init_line_handler();
	}
	explicit ShortScript(const char *source) {
		this->source = std::string(source);
		size = this->source.size();
		init_line_handler();
	}
	// defined in short_script.cpp
	void output_source(size_t);
	
	bool run() {
		parser.init();
		for (size_t i = 0; i < 52; i++) var_set[i] = false;
		try {
			this->compile_statement(this->compiled_statements);
		} catch (const SyntaxError &error) {
			while (parser.stack_trace.size()) {
				parser.stack_trace.top().output(this);
				parser.stack_trace.pop();
			}
			error.output(this);
			return false;
		} catch (const Fatal &error) {
			error.output(this);
		}
		
		try {
			return run_statements(this->compiled_statements);
		} catch (const SyntaxError &error) {
			error.output(this);
		} catch (const Fatal &error) {
			error.output(this);
		} catch (const RuntimeError &error) {
			while (parser.stack_trace.size()) {
				parser.stack_trace.top().output(this);
				parser.stack_trace.pop();
			}
			error.output(this);
		}
		return false;
	}
	void debug() {
		std::cerr << std::endl;
		for (size_t i = 0; i < 52; i++) {
			if (var_set[i]) {
				char name = index_to_char(i);
				std::cerr << name << ":";
				vars[i].output(stderr);
				std::cerr << std::endl;
			}
		}
	}
};
