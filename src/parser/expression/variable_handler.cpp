#include <iostream>
#include "parser.h"
#include "utils.h"

void ExpressionParser::set_var(char name, const std::vector<std::pair<size_t, Variable> > &indexes, const Variable &var) {
	if (name == '_') {
		if (indexes.size()) throw RuntimeError("[] cannot applied to _", indexes[0].first);
		var.output();
		return;
	}
	size_t var_index = char_to_index(name);
	if (indexes.size()) {
		if (!var_set[var_index])
			throw RuntimeError("[] applied to an undefined variable:"s + name, indexes[0].first);
		vars[var_index].array_set(indexes, var);
	} else {
		vars[var_index] = var;
		var_set[var_index] = true;
	}
}
void ExpressionParser::set_var(char name, const std::vector<std::pair<size_t, Variable> > &indexes, Variable &&var) {
	if (name == '_') {
		if (indexes.size()) throw RuntimeError("[] cannot applied to _", indexes[0].first);
		var.output();
		return;
	}
	size_t var_index = char_to_index(name);
	if (indexes.size()) {
		if (!var_set[var_index])
			throw RuntimeError("[] applied to an undefined variable:"s + name, indexes[0].first);
		vars[var_index].array_set(indexes, var);
	} else {
		vars[var_index] = std::move(var);
		var_set[var_index] = true;
	}
}
#ifdef _WIN32
#define fast_getchar _getchar_nolock
#else
#define fast_getchar getchar_unlocked
#endif
// for faster input
static inline std::pair<bool, long long> read_integer() {
	long long res = 0;
	int chr;
	bool negative = false;
	bool read = false;
	// skip whitespaces
	while (1) {
		chr = fast_getchar();
		if (chr == '-') {
			negative = true;
			break;
		}
		if (chr >= '0' && chr <= '9') {
			res = chr - '0';
			read = true;
			break;
		}
		if (chr == ' ' || chr == '\n' || chr == '\r' || chr == '\t' || chr == '\f' || chr == '\v') continue;
		return {false, 0}; // non-whitespace char
	}
	// read until reaching non-numeric
	while (1) {
		chr = fast_getchar();
		if (chr < '0' || chr > '9') {
			ungetc(chr, stdin);
			break;
		}
		res = res * 10 + chr - '0';
		read = true;
	}
	if (!read) return {false, 0};
	if (negative) res = -res;
	return {true, res};
}
void ExpressionParser::get_input(Variable &var) const {
	if (var.type == Variable::Type::INTEGER) {
		auto res = read_integer();
		if (!res.first) throw InputError("Integer");
		new(&var) Variable(res.second);
	} else if (var.type == Variable::Type::FLOAT) {
		double n;
		if (scanf("%lf", &n) != 1) throw InputError("Float");
		new(&var) Variable(n);
	} else if (var.type == Variable::Type::STRING) {
		std::string s;
		if (std::cin >> s) new(&var) Variable(s);
		else throw InputError("String");
	} else if (var.type == Variable::Type::ARRAY) {
		auto n_ = read_integer();
		if (!n_.first) throw InputError("Integer for Array size");
		long long n = n_.second;
		if (n < 0) n = 0;
		new(&var) Variable(std::vector<Variable>());
		var.array.reserve(n);
		for (size_t i = 0; i < (size_t) n; i++) {
			auto cur = read_integer();
			if (!cur.first) throw InputError("Integer for Array content");
			var.array.emplace_back(cur.second);
		}
	} else if (var.type == Variable::Type::ADVANCED_ARRAY) {
		auto n_ = read_integer();
		if (!n_.first) throw InputError("Integer for Advanced Array size");
		long long n = n_.second;
		if (n < 0) n = 0;
		new(&var) Variable(AdvancedArray());
		for (size_t i = 0; i < (size_t) n; i++) {
			auto cur = read_integer();
			if (!cur.first) throw InputError("Integer for Array content");
			var.advanced_array.push_back(Variable(cur.second));
		}
	} else if (var.type == Variable::Type::SET) {
		auto n_ = read_integer();
		if (!n_.first) throw InputError("Integer for Set size");
		long long n = n_.second;
		if (n < 0) n = 0;
		new(&var) Variable(Set());
		for (size_t i = 0; i < (size_t) n; i++) {
			auto cur = read_integer();
			if (!cur.first) throw InputError("Integer for Set content");
			var.set.insert(Variable(cur.second));
		}
	} else assert(0);
}
char ExpressionParser::get_next_counter_name() {
	char cur = 'i';
	size_t var_index = char_to_index(cur);
	while (var_set[var_index]) {
		if (cur == 'z') return '\0'; // no unused variable
		cur++;
		var_index++;
	}
	return cur;
}
