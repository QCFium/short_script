#include <cmath>
#include <climits>
#include <cassert>
#include <algorithm>
#include <iostream>
#include "error/error.h"
#include "variable.h"
#include "operators.h"
#include "parser/expression/parser.h"

static const std::vector<Operator> advanced_operators = {
	OP_MAX,
	OP_MIN,
	OP_LOWER_BOUND,
	OP_UPPER_BOUND,
	OP_COUNT,
};

// determine priority
const std::vector<std::vector<Operator> > op_groups = {
	{OP_MUL, OP_DIV, OP_REM},
	{OP_ADD, OP_SUB},
	advanced_operators,
	{OP_AND, OP_OR, OP_XOR},
	{OP_GT, OP_LT, OP_EQ},
	{OP_COMMA, OP_QUOT}
};
const std::vector<Operator> operators = {
	OP_ADD, OP_SUB, OP_MUL, OP_DIV, OP_REM,
	OP_AND, OP_OR, OP_XOR,
	OP_GT, OP_LT, OP_EQ,
	OP_COMMA, OP_QUOT,
	OP_NEG, OP_NOT, OP_ABS, OP_NYORO,
	OP_DECODE_CHAR,
	OP_MAX, OP_MIN, OP_LOWER_BOUND, OP_UPPER_BOUND, OP_COUNT
};
const std::vector<op_func_t *> ptr_supported = {
	&Operators::array_get,
	&Operators::lower_bound,
	&Operators::upper_bound,
	&Operators::select_min,
	&Operators::select_max,
	&Operators::count
};

// for simple code
#define Type Variable::Type

/* ----------- below are actual implementations of each operators ----------- */
// Operators::* stores the result into the first argument for less constructions of Variable

void Operators::add(Variable &x, const Variable &y) {
	if (x.type == Type::FLOAT && y.type == Type::FLOAT) x.floating += y.floating;
	else if (x.type == Type::FLOAT && y.type == Type::INTEGER) x.floating += y.integer;
	else if (x.type == Type::INTEGER && y.type == Type::FLOAT) x = Variable(x.integer + y.floating);
	else if (x.type == Type::INTEGER && y.type == Type::INTEGER) x.integer += y.integer;
	
	else if (x.type == Type::STRING) x.str += (std::string) y;
	else if (y.type == Type::STRING) x = Variable(((std::string) x) + y.str);
	
	else throw UndefinedOperator("+", x.type, y.type);
}

void Operators::sub(Variable &x, const Variable &y) {
	if (x.type == Type::FLOAT && y.type == Type::FLOAT) x.floating -= y.floating;
	else if (x.type == Type::FLOAT && y.type == Type::INTEGER) x.floating -= y.integer;
	else if (x.type == Type::INTEGER && y.type == Type::FLOAT) x = Variable(x.integer - y.floating);
	else if (x.type == Type::INTEGER && y.type == Type::INTEGER) x.integer -= y.integer;
	
	else if (x.type == Type::SET && y.type == Type::SET) x.set -= y.set;
	
	else throw UndefinedOperator("-", x.type, y.type);
}

void Operators::mul(Variable &x, const Variable &y) {
	if (x.type == Type::FLOAT && y.type == Type::FLOAT) x.floating *= y.floating;
	else if (x.type == Type::FLOAT && y.type == Type::INTEGER) x.floating *= y.integer;
	else if (x.type == Type::INTEGER && y.type == Type::FLOAT) x = Variable(x.integer * y.floating);
	else if (x.type == Type::INTEGER && y.type == Type::INTEGER) x.integer *= y.integer;
	else if (x.type == Type::STRING && (y.type == Type::INTEGER || y.type == Type::FLOAT)) {
		long long num = (long long) y;
		if (num == 0) {
			x.str = "";
		} else {
			if (num < 0) std::reverse(x.str.begin(), x.str.end());
			num = std::abs(num);
			size_t size = x.str.size();
			if (size <= 1) return;
			x.str.resize(x.str.size() * num);
			for (size_t i = 0; i < (size_t) (num - 1); i++)
				std::copy(x.str.begin(), x.str.begin() + size, x.str.begin() + size * (i + 1));
		}
	} else if (x.type == Type::ARRAY && (y.type == Type::INTEGER || y.type == Type::FLOAT)) {
		long long num = (long long) y;
		if (num == 0) {
			x.array = {};
		} else {
			if (num < 0) std::reverse(x.array.begin(), x.array.end());
			num = std::abs(num);
			size_t size = x.array.size();
			if (size <= 1) return;
			x.array.reserve(x.array.size() * num);
			for (size_t i = 0; i < (size_t) (num - 1); i++)
				for (size_t j = 0; j < size; j++)
					x.array.push_back(x.array[j]);
		}
	} else if (x.type == Type::ADVANCED_ARRAY && (y.type == Type::INTEGER || y.type == Type::FLOAT)) {
		long long num = (long long) y;
		x.advanced_array.repeat(num);
	} else throw UndefinedOperator("*", x.type, y.type);
}

void Operators::div(Variable &x, const Variable &y) {
	if (x.type == Type::FLOAT || x.type == Type::INTEGER) {
		if (y.type == Type::FLOAT && y.floating == 0.0) throw RuntimeError("division by zero");
		if (y.type == Type::INTEGER && !y.integer) throw RuntimeError("division by zero");
	}
	
	// arithmetic operations
	if (x.type == Type::FLOAT && y.type == Type::FLOAT) x.floating /= y.floating;
	else if (x.type == Type::FLOAT && y.type == Type::INTEGER) x.floating /= y.integer;
	else if (x.type == Type::INTEGER && y.type == Type::FLOAT) x = Variable(x.integer / y.floating);
	else if (x.type == Type::INTEGER && y.type == Type::INTEGER) {
		long long rem = x.integer % y.integer;
		if (x.integer < 0 && rem) rem += std::abs(y.integer);
		x.integer = (x.integer - rem) / y.integer;
	}
	
	// splitting for String and Array
	else if (x.type == Type::STRING) {
		long long chr = (long long) y;
		std::string str = x.str;
		x = Variable(std::vector<Variable>());
		size_t match_num = std::count_if(str.begin(), str.end(), [&chr](char c) { return c == chr; });
		x.array.reserve(match_num + 1);
		size_t begin = 0;
		for (size_t i = 0; i < str.size(); i++)
			if (str[i] == chr) {
				x.array.push_back(Variable(str.substr(begin, i - begin)));
				begin = i + 1;
			}
		
		x.array.push_back(Variable(str.substr(begin, str.size())));
	} else if (x.type == Type::ARRAY) {
		std::vector<size_t> size;
		size_t begin = 0;
		for (size_t i = 0; i < x.array.size(); i++) {
			try {
				if (x.array[i] == y) size.push_back(i - begin), begin = i + 1;
			} catch (RuntimeError &error) {
				error.set_pos(-1); // dummy one to tell outer error handler that the error occurred in inner operator
				throw;
			}
		}
		size.push_back(x.array.size() - begin);
		
		std::vector<Variable> res;
		res.resize(size.size(), Variable(std::vector<Variable>()));
		size_t head = 0;
		for (size_t i = 0; i < size.size(); i++) {
			res[i].array.reserve(size[i]);
			std::copy(x.array.begin() + head,
					  x.array.begin() + head + size[i], std::back_inserter(res[i].array));
			head += size[i] + 1;
		}
		x.array.swap(res);
	} else if (x.type == Type::ADVANCED_ARRAY) {
		std::vector<Variable> res(1, Variable(AdvancedArray()));
		for (size_t i = 0; i < x.advanced_array.size(); i++) {
			try {
				if (x.advanced_array[i] == y) {
					res.push_back(Variable(AdvancedArray()));
				} else {
					res.back().advanced_array.push_back(x.advanced_array[i]);
				}
			} catch (RuntimeError &error) {
				error.set_pos(-1); // dummy one to tell outer error handler that the error occurred in inner operator
				throw;
			}
		}
		x = Variable(std::vector<Variable>());
		x.array.swap(res);
	} else throw UndefinedOperator("/", x.type, y.type);
}

void Operators::rem(Variable &x, const Variable &y) {
	if (x.type == Type::FLOAT || x.type == Type::INTEGER) {
		if (y.type == Type::FLOAT && y.floating == 0.0) throw RuntimeError("division by zero");
		if (y.type == Type::INTEGER && !y.integer) throw RuntimeError("division by zero");
	}
	
	// arithmetic operations
	if (x.type == Type::FLOAT && y.type == Type::FLOAT) x.floating = fmod(x.floating, y.floating);
	else if (x.type == Type::FLOAT && y.type == Type::INTEGER) x.floating = fmod(x.floating, (double) y.integer);
	else if (x.type == Type::INTEGER && y.type == Type::FLOAT) x = Variable(fmod((double)x.integer, y.floating));
	else if (x.type == Type::INTEGER && y.type == Type::INTEGER) {
		x.integer %= y.integer;
		if (x.integer < 0) x.integer += std::abs(y.integer);
	}
	
	// count
	else if (x.type == Type::SET) x = Variable((long long) x.set.count(y));
	else if (x.type == Type::ARRAY) {
		size_t cnt = 0;
		for (auto &var : x.array) {
			try {
				if (var == y) cnt++;
			} catch (RuntimeError &error) {
				error.set_pos(-1); // dummy
				throw;
			}
		}
		x = Variable((long long) cnt);
	} else if (x.type == Type::ADVANCED_ARRAY) {
		size_t cnt = 0;
		std::vector<Variable> all;
		x.advanced_array.get_all(all);
		for (auto &var : all) {
			try {
				if (var == y) cnt++;
			} catch (RuntimeError &error) {
				error.set_pos(-1); // dummy
				throw;
			}
		}
		x = Variable((long long) cnt);
	} else if (x.type == Type::STRING) {
		long long y_int = (long long) y;
		x = Variable((long long) std::count_if(x.str.begin(), x.str.end(), [&](char c) -> bool {
			return c == y_int;
		}));
	} else throw UndefinedOperator("%", x.type, y.type);
}

void Operators::and_(Variable &x, const Variable &y) {
	if (x.type == Type::INTEGER || y.type == Type::INTEGER)
		x.integer &= y.integer;
	else if (x.type == Type::SET || y.type == Type::SET)
		x.set &= y.set;
	else throw UndefinedOperator("&", x.type, y.type);
}
void Operators::or_(Variable &x, const Variable &y) {
	if (x.type == Type::INTEGER || y.type == Type::INTEGER)
		x.integer |= y.integer;
	else if (x.type == Type::SET || y.type == Type::SET)
		x.set |= y.set;
	else throw UndefinedOperator("|", x.type, y.type);
}
void Operators::xor_(Variable &x, const Variable &y) {
	if (x.type == Type::INTEGER || y.type == Type::INTEGER)
		x.integer ^= y.integer;
	else if (x.type == Type::SET || y.type == Type::SET)
		x.set ^= y.set;
	else throw UndefinedOperator("^", x.type, y.type);
}

// comparision operators for internal use
bool operator < (const Variable &x_, const Variable &y_) {
	const Variable &x = x_.type == Type::PTR ? *x_.ptr : x_;
	const Variable &y = y_.type == Type::PTR ? *y_.ptr : y_;
	
	if (x.type == Type::INTEGER && y.type == Type::INTEGER) return x.integer < y.integer;
	if (x.type == Type::INTEGER && y.type == Type::FLOAT) return x.integer < y.floating;
	if (x.type == Type::FLOAT && y.type == Type::INTEGER) return x.floating < y.integer;
	if (x.type == Type::FLOAT && y.type == Type::FLOAT) return x.floating < y.floating;
	if (x.type == Type::STRING && y.type == Type::STRING) return x.str < y.str;
	if (x.type == Type::ARRAY && y.type == Type::ARRAY) {
		size_t common_size = std::min(x.array.size(), y.array.size());
		for (size_t i = 0; i < common_size; i++) {
			try {
				if (x.array[i] != y.array[i])
					return x.array[i] < y.array[i];
			} catch (RuntimeError &error) {
				error.set_pos(-1); // dummy
				throw;
			}
		}
		return x.array.size() < y.array.size();
	} else if (x.type == Type::ADVANCED_ARRAY && y.type == Type::ADVANCED_ARRAY) {
		size_t common_size = std::min(x.advanced_array.size(), y.advanced_array.size());
		for (size_t i = 0; i < common_size; i++) {
			try {
				if (x.advanced_array[i] != y.advanced_array[i])
					return x.advanced_array[i] < y.advanced_array[i];
			} catch (RuntimeError &error) {
				error.set_pos(-1); // dummy
				throw;
			}
		}
		return x.advanced_array.size() < y.advanced_array.size();
	}
	throw UndefinedOperator("<", x.type, y.type);
}
bool operator > (const Variable &x, const Variable &y) {
	return y < x;
}
bool operator == (const Variable &x_, const Variable &y_) {
	const Variable &x = x_.type == Type::PTR ? *x_.ptr : x_;
	const Variable &y = y_.type == Type::PTR ? *y_.ptr : y_;
	
	if (x.type == Type::INTEGER && y.type == Type::INTEGER) return x.integer == y.integer;
	if (x.type == Type::INTEGER && y.type == Type::FLOAT) return std::abs(x.integer - y.floating) <= 1e-10;
	if (x.type == Type::FLOAT && y.type == Type::INTEGER) return std::abs(x.floating - y.integer) <= 1e-10;
	if (x.type == Type::FLOAT && y.type == Type::FLOAT) return std::abs(x.floating - y.floating) <= 1e-10;
	
	if (x.type == Type::STRING && y.type == Type::STRING) return x.str == y.str;
	if (x.type == Type::ARRAY && y.type == Type::ARRAY) {
		if (x.array.size() != y.array.size()) return false;
		for (size_t i = 0; i < x.array.size(); i++) {
			try {
				if (x.array[i] != y.array[i]) return false;
			} catch (RuntimeError &error) {
				error.set_pos(-1); // dummy
				throw;
			}
		}
		return true;
	}
	if (x.type == Type::ADVANCED_ARRAY && y.type == Type::ADVANCED_ARRAY) {
		if (x.advanced_array.size() != y.advanced_array.size()) return false;
		for (size_t i = 0; i < x.advanced_array.size(); i++) {
			try {
				if (x.advanced_array[i] != y.advanced_array[i]) return false;
			} catch (RuntimeError &error) {
				error.set_pos(-1); // dummy
				throw;
			}
		}
		return true;
	}
	throw UndefinedOperator("=", x.type, y.type);
}
bool operator != (const Variable &x, const Variable &y) {
	return !(x == y);
}


void Operators::greater(Variable &x, const Variable &y) {
	x = Variable((long long) (x > y));
}
void Operators::less(Variable &x, const Variable &y) {
	x = Variable((long long) (x < y));
}
void Operators::equal(Variable &x, const Variable &y) {
	x = Variable((long long) (x == y));
}

void Operators::array_get(Variable &x_, const Variable &y) {
	bool is_x_ptr = (x_.type == Variable::Type::PTR);
	const Variable &x = (is_x_ptr ? *x_.ptr : x_);
	if (y.type == Type::ARRAY) { // a[1,2] : interval
		if (!y.array.size()) throw RuntimeError("empty Array for interval");
		long long start = (long long) y.array[0];
		if (start < 0) throw RuntimeError("Negative interval start : " + std::to_string(start));
		long long len = -1;
		if (y.array.size() > 1) len = (long long) y.array[1];
		if (len < 0) throw RuntimeError("Negative interval length : " + std::to_string(len));
		
		size_t x_len;
		if (x.type == Type::INTEGER) x_len = CHAR_BIT * sizeof(long long);
		else if (x.type == Type::STRING) x_len = x.str.size();
		else if (x.type == Type::ARRAY) x_len = x.array.size();
		else if (x.type == Type::ADVANCED_ARRAY) x_len = x.advanced_array.size();
		else if (x.type == Type::SET) x_len = x.set.size();
		else throw UndefinedOperator("[", x.type, y.type);
		
		if ((size_t) start > x_len) throw RangeError(type_to_str(x.type), x_len, start);
		
		size_t end;
		if (len != -1) end = std::min(x_len, (size_t)(start + len));
		else end = x_len;
		
		if (x.type == Type::INTEGER) {
			if (end == (size_t) start) {
				if (is_x_ptr) x_ = Variable(0LL);
				else x_.integer = 0;
			} else if (end == x_len) {
				if (is_x_ptr) x_ = Variable((long long)((unsigned long long)x.integer & ~((1ULL << start) - 1ULL)));
				else x_.integer = (unsigned long long)x_.integer & ~((1ULL << start) - 1ULL);
			} else {
				if (is_x_ptr) x_ = Variable((long long)((unsigned long long)x.integer & ((1ULL << end) - (1ULL << start))));
				else x_.integer = (unsigned long long)x_.integer & ((1ULL << end) - (1ULL << start));
			}
		} else if (x.type == Type::STRING) x_ = Variable(x.str.substr(start, end - start));
		else if (x.type == Type::ARRAY) x_ = Variable(std::vector<Variable>(x.array.begin() + start, x.array.begin() + end));
		else if (x.type == Type::ADVANCED_ARRAY) {
			/***************** Could be speeded up *****************/
			AdvancedArray res;
			for (size_t i = start; i < end; i++) res.push_back(x.advanced_array[i]);
			if (is_x_ptr) x_ = Variable(res);
			else x_.advanced_array = std::move(res);
		} else if (x.type == Type::SET) {
			/***************** Could be speeded up *****************/
			Set res(x.set.is_multi);
			for (size_t i = start; i < end; i++) res.insert(x.set[i]);
			if (is_x_ptr) x_ = Variable(res);
			else x_.set = std::move(res);
		} else assert(0);
	} else { // normal subscription
		long long index = (long long) y;
		if (x.type == Type::INTEGER) {
			if (index < 0 || index >= (long long)(CHAR_BIT * sizeof(long long)) - 1)
				throw RangeError("Integer", CHAR_BIT * sizeof(long long) - 1, index);
			bool set = x.integer & (1LL << index);
			if (is_x_ptr) x_ = Variable((long long) set);
			else x_.integer = set;
		} else if (x.type == Type::STRING) {
			if (index < 0 || ((size_t) index) >= x.str.size())
				throw RangeError("String", x.str.size(), index);
			x_ = Variable((long long) x.str.at(index));
		} else if (x.type == Type::ARRAY) {
			if (index < 0 || ((size_t) index) >= x.array.size())
				throw RangeError("Array", x.array.size(), index);
			if (is_x_ptr) x_ = Variable(&x.array.at(index));
			else x_ = Variable(x.array.at(index));
		} else if (x.type == Type::ADVANCED_ARRAY) {
			if (index < 0 || ((size_t) index) >= x.advanced_array.size())
				throw RangeError("Advanced Array", x.advanced_array.size(), index);
			if (is_x_ptr) x_ = Variable(&x.advanced_array[index]);
			else x_ = Variable(x.advanced_array[index]);
		} else if (x.type == Type::SET) x_ = x.set[index];
		else throw UndefinedOperator("[", x.type, y.type);
	}
}

void Operators::comma(Variable &x, const Variable &y) {
	size_t x_depth = x.depth();
	size_t y_depth = y.depth();
	if (x_depth == y_depth) x = Variable(std::vector<Variable>({x,y})); // make new array with the 2 variables
	else if (x_depth + 1 == y_depth) { // push front
		if (y.type == Type::ARRAY) {
			std::vector<Variable> res(1 + y.array.size());
			res[0] = x;
			for (size_t i = 0; i < y.array.size(); i++)
				res[i + 1] = y.array[i];
			x = Variable(std::vector<Variable>());
			x.array.swap(res);
		} else if (y.type == Type::ADVANCED_ARRAY) {
			AdvancedArray res;
			res.push_back(x);
			for (size_t i = 0; i < y.advanced_array.size(); i++)
				res.push_back(y.advanced_array[i]);
			x = Variable(res);
		} else assert(0);
	} else if (x_depth == y_depth + 1) { // push back
		if (x.type == Type::ARRAY) x.array.push_back(y);
		else if (x.type == Type::ADVANCED_ARRAY) x.advanced_array.push_back(y);
		else assert(0);
	} else throw RuntimeError("operator ',' with more than 1 depth difference is unsupported");
}

void Operators::quot(Variable &x, const Variable &y) {
	if (x.type == Type::SET && y.type == Type::SET) {
		std::vector<Variable> all;
		y.set.get_all(all);
		for (auto var : all) x.set.insert(var);
	} else if (x.type == Type::SET) {
		x.set.insert(y);
	} else if (y.type == Type::SET) {
		Variable x_bak = x;
		x = Variable(y.set);
		x.set.insert(x_bak);
	} else {
		Variable x_bak = x;
		x = Variable(Set());
		x.set.insert(x_bak);
		x.set.insert(y);
	}
}

/* -------- below are unary operators -------- */

void Operators::neg(Variable &x, const Variable &y) {
	(void) y;
	if (x.type == Type::INTEGER) x.integer = -x.integer;
	else if (x.type == Type::FLOAT) x.floating = -x.floating;
	else throw UndefinedOperator("-", x.type);
}
void Operators::inv(Variable &x, const Variable &y) {
	(void) y;
	x = Variable((long long) ! (long long) x);
}
void Operators::abs(Variable &x, const Variable &y) {
	(void) y;
	if (x.type == Type::INTEGER) x.integer = std::abs(x.integer);
	else if (x.type == Type::FLOAT) x.floating = std::abs(x.floating);
	else throw UndefinedOperator("+", x.type);
}
void Operators::nyoro(Variable &x, const Variable &y) {
	(void) y;
	if (x.type == Type::INTEGER) x.integer = (long long)~(unsigned long long) x.integer;
	else throw UndefinedOperator("~", x.type);
}

void Operators::decode_char(Variable &x, const Variable &y) {
	(void) y;
	if (x.type == Type::INTEGER) {
		if (x.integer < 0 || x.integer > 255) throw RuntimeError("Integer for char out of range : " + std::to_string(x.integer));
		x = Variable(std::string{(char) x.integer});
	} else throw UndefinedOperator("$d", x.type);
}


/* -------- below are advanced operators -------- */
// note that they generally store the result into the first argument

void Operators::select_max(Variable &x_, const Variable &y) {
	const Variable &x = x_.type == Type::PTR ? *x_.ptr : x_;
	
	try {
		if (x < y) x_ = y;
	} catch (RuntimeError &error) {
		error.set_pos(-1); // dummy
		throw;
	}
}
void Operators::select_min(Variable &x_, const Variable &y) {
	const Variable &x = x_.type == Type::PTR ? *x_.ptr : x_;
	
	try {
		if (x > y) x_ = y;
	} catch (RuntimeError &error) {
		error.set_pos(-1); // dummy
		throw;
	}
}

void Operators::lower_bound(Variable &x_, const Variable &y) {
	const Variable &x = (x_.type == Type::PTR ? *x_.ptr : x_);
	
	if (x.type == Type::ARRAY) { // assume it's sorted, binary search
		int low = -1;
		int high = x.array.size();
		while (high - low > 1) {
			int mid = low + (high - low) / 2;
			try {
				if (x.array[mid] < y) low = mid;
				else high = mid;
			} catch (RuntimeError &error) {
				error.set_pos(-1); // dummy
				throw;
			}
		}
		x_ = Variable((long long) high);
	} else if (x.type == Type::ADVANCED_ARRAY) x_ = Variable((long long) x.advanced_array.lower_bound(y));
	else if (x.type == Type::SET) x_ = Variable((long long) x.set.lower_bound(y));
	else throw UndefinedOperator("$l", x.type, y.type);
}
void Operators::upper_bound(Variable &x_, const Variable &y) {
	const Variable &x = (x_.type == Type::PTR ? *x_.ptr : x_);
	
	if (x.type == Type::ARRAY) { // assume it's sorted, binary search
		int low = -1;
		int high = x.array.size();
		while (high - low > 1) {
			int mid = low + (high - low) / 2;
			try {
				if (x.array[mid] > y) high = mid;
				else low = mid;
			} catch (RuntimeError &error) {
				error.set_pos(-1); // dummy
				throw;
			}
		}
		x_ = Variable((long long) high);
	} else if (x.type == Type::ADVANCED_ARRAY) x_ = Variable((long long) x.advanced_array.upper_bound(y));
	else if (x.type == Type::SET) x_ = Variable((long long) x.set.upper_bound(y));
	else throw UndefinedOperator("$u", x.type, y.type);
}
void Operators::count(Variable &x_, const Variable &y) {
	const Variable &x = (x_.type == Type::PTR ? *x_.ptr : x_);
	
	if (x.type == Type::SET) x_ = Variable((long long) x.set.count(y));
	else if (x.type == Type::ARRAY) {
		size_t cnt = 0;
		for (auto &var : x.array) {
			try {
				if (var == y) cnt++;
			} catch (RuntimeError &error) {
				error.set_pos(-1); // dummy
				throw;
			}
		}
		x_ = Variable((long long) cnt);
	} else if (x.type == Type::ADVANCED_ARRAY) {
		size_t cnt = 0;
		std::vector<Variable> all;
		x.advanced_array.get_all(all);
		for (auto &var : all) {
			try {
				if (var == y) cnt++;
			} catch (RuntimeError &error) {
				error.set_pos(-1); // dummy
				throw;
			}
		}
		x_ = Variable((long long) cnt);
	} else if (x.type == Type::STRING) {
		long long y_int = (long long) y;
		x_ = Variable((long long) std::count_if(x.str.begin(), x.str.end(), [&](char c) -> bool {
			return c == y_int;
		}));
	} else throw UndefinedOperator("%", x.type, y.type);
}
