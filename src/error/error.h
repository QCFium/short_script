#pragma once
#include <string>
#include <variable.h>

class ShortScript;

// error stack tract info
struct ErrorInfo {
	std::string msg;
	size_t pos = -1;
	bool pos_valid;
	ErrorInfo(const std::string &msg) {
		this->msg = msg;
		this->pos_valid = false;
	}
	ErrorInfo(const std::string &msg, size_t pos) {
		this->msg = msg;
		this->pos = pos;
		this->pos_valid = true;
	}
	void output(ShortScript *script) const;
};

// syntax errors
struct SyntaxError {
	std::string msg;
	size_t pos;
	// pos == -1 for EOF
	explicit SyntaxError (const std::string &msg, size_t pos) { // pos == -1 for EOF
		this->msg = msg;
		this->pos = pos;
	}
	virtual void output(ShortScript *script) const;
};
// error that something is expected
struct Expected : public SyntaxError {
	explicit Expected (const std::string &expected, size_t pos) : SyntaxError(expected, pos) {}
	void output(ShortScript *script) const override;
};

// fatal error(internal error)
struct Fatal {
	std::string msg;
	size_t pos;
	explicit Fatal (const std::string &msg, size_t pos) {
		this->msg = msg;
		this->pos = pos;
	}
	void output(ShortScript *script) const;
};

// runtime errors
struct RuntimeError {
	std::string msg;
	bool pos_valid = false;
	size_t pos = -1;
	explicit RuntimeError (const std::string &msg) {
		this->msg = msg;
	}
	explicit RuntimeError (const std::string &msg, size_t pos) {
		this->msg = msg;
		this->pos = pos;
		pos_valid = true;
	}
	void output(ShortScript *script) const;
	void set_pos(size_t pos, bool force = false) {
		if (!force && pos_valid) return; // don't overwrite unless 'force' specified
		pos_valid = true;
		this->pos = pos;
	}
};
// templates
RuntimeError RangeError(const std::string &obj, size_t size, long long index);
RuntimeError UndefinedOperator(const std::string &op, Variable::Type first, Variable::Type second);
RuntimeError UndefinedOperator(const std::string &op, Variable::Type first);
RuntimeError CastError(Variable::Type from, Variable::Type to);
RuntimeError InputError(const std::string &content);
