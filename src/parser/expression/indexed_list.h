#pragma once
#include <vector>
#include <list>
#include <iterator>

// like std::list, but each element has const index and they are erase only
template<typename T>
class indexed_list {
private:
	struct Content {
		T content;
		size_t prev;
		size_t next;
		Content(const T &content, size_t prev, size_t next) : content(content), prev(prev), next(next) {};
	};
	std::vector<Content> data;
	size_t _begin = -1;
	size_t _end = -1;
	size_t _size = 0; // excluding erase element, different from data.size()

public:
	explicit indexed_list (const std::list<T> &list) {
		data.reserve(list.size());
		for (auto &i : list) data.push_back({i, (size_t) -1, (size_t) -1});
		_size = list.size();
		for (size_t i = 0; i + 1 < _size; i++) {
			data[i].next = i + 1;
			data[i + 1].prev = i;
		}
		_begin = _size ? 0 : -1;
		_end = _size ? _size - 1 : -1;
	}
	indexed_list () {}
	explicit operator std::list<T> () const {
		std::list<T> res;
		for (size_t cur = _begin; cur != (size_t) -1; cur = data[cur].next)
			res.push_back(data[cur].content);
		return res;
	}
	
	struct iterator : std::iterator<std::bidirectional_iterator_tag, T> {
		indexed_list *list = nullptr;
		size_t pos;
		
		iterator (indexed_list *list, size_t pos) : list(list), pos(pos) {};
		iterator (void) {}
		inline iterator &operator ++ (void) {
			assert(pos != (size_t) -2);
			if (pos == (size_t) -1) pos = list->_begin;
			else pos = list->data[pos].next;
			return *this;
		}; // ++itr
		inline iterator &operator -- (void) {
			assert(pos != (size_t) -2);
			if (pos == (size_t) -1) pos = list->_end;
			else pos = list->data[pos].prev;
			return *this;
		}; // --itr
		inline iterator operator ++ (int) {
			auto res = *this;
			++*this;
			return res;
		}
		inline iterator operator -- (int) {
			auto res = *this;
			--*this;
			return res;
		}
		inline bool operator < (const iterator &itr) const { return pos < itr.pos; }
		inline bool operator > (const iterator &itr) const { return pos < itr.pos; }
		inline bool operator == (const iterator &itr) const { return pos == itr.pos; }
		inline bool operator != (const iterator &itr) const { return pos != itr.pos; }
		inline T &operator * (void) const { return list->data[pos].content; }
		inline T *operator -> (void) const { return &list->data[pos].content; }
	};
	
	void fit() { // actually remove erased elements(O(N))
		std::vector<Content> new_data;
		for (auto &i : data) new_data.push_back(i);
		_size = new_data.size();
		for (size_t i = 0; i < _size; i++) {
			new_data[i].prev = i ? i - 1 : -1;
			new_data[i].next = i + 1 < _size ? i + 1 : -1;
		}
		std::swap(data, new_data);
		_begin = _size ? 0 : -1;
		_end = _size ? _size - 1 : -1;
	}
	iterator erase(size_t pos) {
		assert(pos < data.size());
		assert(data[pos].prev != (size_t) -2);
		assert(data[pos].next != (size_t) -2);
		size_t prev = data[pos].prev;
		size_t next = data[pos].next;
		if (prev != (size_t) -1) data[prev].next = next;
		else _begin = next;
		if (next != (size_t) -1) data[next].prev = prev;
		else _end = prev;
		data[pos].prev = (size_t) -2;
		data[pos].next = (size_t) -2;
		_size--;
		return iterator(this, next);
	}
	inline iterator erase(const iterator &itr) { return erase(itr.pos); }
		iterator erase(const iterator &start, const iterator &end) { // erase [start,end)
		iterator cur = start;
		while (cur != end) {
			erase(cur++);
		}
		return end;
	}
	void push_back(const T &element) {
		if (_end == (size_t) -1) {
			assert(!_size);
			_begin = _end = 0;
			data.clear();
		} else data[_end].next = data.size();
		data.push_back({element, _end, (size_t) -1});
		_end = data.size() - 1;
		_size++;
	}
	void reset_erased() { // take back erased elements
		if (!data.size()) return;
		_size = data.size();
		_begin = 0;
		_end = _size - 1;
		for (size_t i = 0; i < _size; i++) {
			data[i].prev = i ? i - 1 : -1;
			data[i].next = (i + 1 < _size) ? i + 1 : -1;
		}
	}
	inline T &back(void) { return data[_end].content; }
	inline const T &back(void) const { return data[_end].content; }
	inline iterator begin(void) { return iterator(this, _begin); }
	inline iterator end(void) { return iterator(this, -1); }
	inline size_t size() const { return _size; }
	inline iterator index(size_t pos) { return iterator(this, pos); }
};
