#include <iostream>
#include "error.h"
#include "color/colors.h"
#include "short_script.h"

std::string get_pos_str(size_t pos, ShortScript *script) {
	std::string res = " @ ";
	if (pos >= script->size) res += "EOF";
	else res += std::to_string(script->line_num[pos] + 1) + ":" +
		std::to_string(script->pos_in_line[pos] + 1); // 1-indexed
	return res;
}

void ErrorInfo::output(ShortScript *script) const {
	if (pos_valid) {
		std::cerr << CHAR_CYAN << "Info" << COLOR_RESET << get_pos_str(pos, script) << " : " << msg << std::endl;
		script->output_source(pos);
	} else std::cerr << CHAR_CYAN << "Info" << COLOR_RESET << " : " << msg << std::endl;
}

void SyntaxError::output(ShortScript *script) const {
	std::cerr << CHAR_RED << "Syntax Error" << COLOR_RESET << get_pos_str(pos, script) << " : " << msg << std::endl;
	script->output_source(pos);
}
void Expected::output(ShortScript *script) const {
	std::cerr << CHAR_RED << "Syntax Error" << COLOR_RESET << get_pos_str(pos, script) << " : expected " << msg << std::endl;
	script->output_source(pos);
}

void Fatal::output(ShortScript *script) const {
	std::cerr << "\a\a\a";
	std::cerr << CHAR_PURPLE << "FATAL" << COLOR_RESET << get_pos_str(pos, script) << " : " << msg << std::endl;
	std::cerr << CHAR_YELLOW << "This is a Fatal Error. This should not happen, even if the script was wrong." << COLOR_RESET << std::endl;
	std::cerr << "Please contact the developer" << std::endl;
}

void RuntimeError::output(ShortScript *script) const {
	if (pos_valid) {
		std::cerr << CHAR_RED << "Runtime Error" << COLOR_RESET << get_pos_str(pos, script) << " : " << msg << std::endl;
		script->output_source(pos);
	} else std::cerr << CHAR_RED << "Runtime Error" << COLOR_RESET << " : " << msg << std::endl;
}

RuntimeError RangeError(const std::string &obj, size_t size, long long index) {
	return RuntimeError("access to " + obj + " out of range" + "\n" +
			"size : " + std::to_string(size) + "\n" +
			"index : " + std::to_string(index));
}
RuntimeError UndefinedOperator(const std::string &op, Variable::Type first, Variable::Type second) {
	return RuntimeError("undefined operator " + op + " between " + type_to_str(first) +
		" and " + type_to_str(second));
}
RuntimeError UndefinedOperator(const std::string &op, Variable::Type first) {
	return RuntimeError("undefined operator " + op + " to " + type_to_str(first));
}
RuntimeError CastError(Variable::Type from, Variable::Type to) {
	return RuntimeError("cannot cast from " + type_to_str(from) + " to " + type_to_str(to));
}
RuntimeError InputError(const std::string &content) {
	return RuntimeError("could not read " + content + " from stdin");
}
