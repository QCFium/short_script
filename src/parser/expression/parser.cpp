#include "parser.h"

// declared in parser.h
const std::map<char, Variable::Type> type_of_specifier = {
	{'d', Variable::Type::INTEGER},
	{'f', Variable::Type::FLOAT},
	{'s', Variable::Type::STRING},
	{'a', Variable::Type::ARRAY},
	{'A', Variable::Type::ADVANCED_ARRAY},
	{'t', Variable::Type::SET}
};

