#pragma once
#include <iostream>

inline void output_ll(FILE *stream, const long long n) {
	static char buf[32];
	char *ptr = buf + 31;
	long long abs = std::abs(n);
	if (abs) {
		while (abs) {
			*--ptr = '0' + abs % 10;
			abs /= 10;
		}
	} else *--ptr = '0';
	if (n < 0) *--ptr = '-';
	fwrite(ptr, 1, buf + 31 - ptr, stream);
}

// for variable name
inline char index_to_char(size_t index) {
	if (index < 26) return index + 'a';
	else if (index < 52) return index - 26 + 'A';
	else assert(0);
}

inline size_t char_to_index(char c) {
	size_t res = c - 'a';
	if (res < 26) return res;
	else return c - 'A' + 26;
}
