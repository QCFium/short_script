#include <cassert>
#include <iostream>
#include <algorithm>
#include "advanced_array.h"
#include "variable.h"
#include "parser/operators.h"

AdvancedArray::AdvancedArray() {
	nodes.push_back(Node()); // this dummy node will only have the left child
}

AdvancedArray::AdvancedArray(const std::vector<Variable> &array) : AdvancedArray() {
	_size = array.size();
	if (array.size()) {
		size_t root = _init_copy(&array[0], &array[0] + array.size(), 0);
		nodes[0].left = root;
	}
}
// returns the index of root node constructed
size_t AdvancedArray::_init_copy(const Variable *start, const Variable *end, size_t parent) { // construct tree with elements in [start, end)
	size_t size = end - start;
	assert(size);
	const Variable *mid = start + (size >> 1);
	
	// root
	size_t cur_node = nodes.size();
	nodes.push_back(Node(pool.size(), parent));
	pool.push_back(*mid);
	
	// left child
	if (mid != start) { // left construction
		size_t left = _init_copy(start, mid, cur_node);
		nodes[cur_node].left = left;
	}
	if (mid + 1 != end) { // right construction
		size_t right = _init_copy(mid + 1, end, cur_node);
		nodes[cur_node].right = right;
	}
	update_node_info(cur_node);
	return cur_node;
}

AdvancedArray::AdvancedArray(size_t size, const Variable &var) : AdvancedArray() {
	_size = size;
	if (size) {
		size_t root = _init_fill(size, var, 0);
		nodes[0].left = root;
	}
}
// returns the index of root node constructed
size_t AdvancedArray::_init_fill(size_t size, const Variable &var, size_t parent) {
	assert(size);
	size_t mid = size / 2;
	size_t cur_node = nodes.size();
	nodes.push_back(Node(pool.size(), parent));
	pool.push_back(var);
	
	if (mid) {
		size_t left = _init_fill(mid, var, cur_node);
		nodes[cur_node].left = left;
	}
	if (mid + 1 < size) {
		size_t right = _init_fill(size - mid - 1, var, cur_node);
		nodes[cur_node].right = right;
	}
	update_node_info(cur_node);
	return cur_node;
}

void AdvancedArray::balance(size_t node, bool is_remove) {
	size_t cur = node;
	while (nodes[cur].parent) {
		size_t next = nodes[cur].parent;
		size_t prev_height = nodes[next].subtree_height;
		if ((nodes[next].left == cur) != is_remove) {
			// left increased
			if (height_sub(next) == 2) {
				if (height_sub(nodes[next].left) >= 0) next = rotate_r(next);
				else next = rotate_lr(next);
			} else update_node_info(next);
		} else {
			// right increased
			if (height_sub(next) == -2) {
				if (height_sub(nodes[next].right) <= 0) next = rotate_l(next);
				else next = rotate_rl(next);
			} else update_node_info(next);
		}
		if (prev_height == nodes[next].subtree_height) break; // height unchanged, further fix is not required
		cur = next;
	}
}
void AdvancedArray::insert(size_t pos, const Variable &val) {
	assert(pos <= _size);
	bool moved_right = false;
	size_t parent = 0;
	size_t cur_node = nodes[parent].left;
	while (cur_node) { // move down the tree
		nodes[cur_node].subtree_size++;
		size_t left = nodes[cur_node].left;
		size_t left_size = nodes[left].subtree_size;
		parent = cur_node;
		if (left_size >= pos) { // move left
			moved_right = false;
			cur_node = left;
		} else {
			pos -= left_size + 1;
			moved_right = true;
			cur_node = nodes[cur_node].right;
		}
	}
	if (moved_right) nodes[parent].right = nodes.size();
	else nodes[parent].left = nodes.size();
	nodes.push_back(Node(pool.size(), parent));
	pool.push_back(val);
	balance(nodes.size() - 1, false);
	_size++;
}
void AdvancedArray::sorted_insert(const Variable &val) {
	bool moved_right = false;
	size_t parent = 0;
	size_t cur_node = nodes[parent].left;
	while (cur_node) { // move down the tree
		nodes[cur_node].subtree_size++;
		parent = cur_node;
		if (pool[nodes[cur_node].val_index] > val) { // move left
			moved_right = false;
			cur_node = nodes[cur_node].left;
		} else {
			moved_right = true;
			cur_node = nodes[cur_node].right;
		}
	}
	if (moved_right) nodes[parent].right = nodes.size();
	else nodes[parent].left = nodes.size();
	nodes.push_back(Node(pool.size(), parent));
	pool.push_back(val);
	balance(nodes.size() - 1, false);
	_size++;
}

void AdvancedArray::remove_index(size_t pos) {
	assert(pos < _size);
	size_t cur_node = nodes[0].left;
	while (cur_node) { // move down the tree
		nodes[cur_node].subtree_size--;
		size_t left = nodes[cur_node].left;
		size_t left_size = nodes[left].subtree_size;
		if (left_size > pos) cur_node = left;
		else if (left_size < pos) {
			pos -= left_size + 1;
			cur_node = nodes[cur_node].right;
		} else { // remove cur_node
			size_t right = nodes[cur_node].right;
			if (!left) {
				replace_subtree(cur_node, right);
				balance(right, true);
			} else {
				size_t nearest_left = left;
				while (nodes[nearest_left].right) {
					nodes[nearest_left].subtree_size--;
					nearest_left = nodes[nearest_left].right;
				}
				nodes[cur_node].val_index = nodes[nearest_left].val_index;
				replace_subtree(nearest_left, nodes[nearest_left].left);
				balance(nodes[nearest_left].left, true);
			}
			_size--;
			return;
		}
	}
	assert(0);
}

auto AdvancedArray::operator [] (size_t index) -> Variable & {
	assert(index < _size);
	size_t cur_node = nodes[0].left;
	while (cur_node) {
		size_t left = nodes[cur_node].left;
		size_t left_size = nodes[left].subtree_size;
		if (left_size > index) cur_node = left;
		else if (left_size < index) {
			index -= left_size + 1;
			cur_node = nodes[cur_node].right;
		} else return pool[nodes[cur_node].val_index];
	}
	assert(0); // unreachable
	while(1);
}
auto AdvancedArray::operator [] (size_t index) const -> const Variable & {
	assert(index < _size);
	size_t cur_node = nodes[0].left;
	while (cur_node) {
		size_t left = nodes[cur_node].left;
		size_t left_size = nodes[left].subtree_size;
		if (left_size > index) cur_node = left;
		else if (left_size < index) {
			index -= left_size + 1;
			cur_node = nodes[cur_node].right;
		} else return pool[nodes[cur_node].val_index];
	}
	assert(0); // unreachable
	while(1);
}

void AdvancedArray::_get_all(std::vector<Variable> &all, size_t node) const {
	if (nodes[node].left) _get_all(all, nodes[node].left);
	all.push_back(pool[nodes[node].val_index]);
	if (nodes[node].right) _get_all(all, nodes[node].right);
}
void AdvancedArray::get_all(std::vector<Variable> &all) const {
	if (!_size) return;
	_get_all(all, nodes[0].left);
}

void AdvancedArray::_output(FILE *stream, size_t node, bool rightest) const {
	if (nodes[node].left) _output(stream, nodes[node].left, false);
	pool[nodes[node].val_index].output();
	if (nodes[node].right || !rightest) fputc(' ', stream);
	if (nodes[node].right) _output(stream, nodes[node].right, rightest);
}
void AdvancedArray::output(FILE *stream) const {
	if (!_size) return;
	_output(stream, nodes[0].left, true);
}

// copy construct from [start, end) (mod array.size())
// returns the index of root node constructed
size_t AdvancedArray::_repeat_fill(const std::vector<Variable> &array, size_t start, size_t end, size_t parent) {
	assert(start != end);
	size_t mid = start + ((end - start) >> 1);
	size_t res_index = nodes.size();
	nodes.push_back(Node(pool.size(), parent));
	pool.push_back(array[mid % array.size()]);
	
	// left child
	if (mid != start) {
		size_t left = _repeat_fill(array, start, mid, res_index);
		nodes[res_index].left = left;
	}
	if (mid + 1 != end) {
		size_t right = _repeat_fill(array, mid + 1, end, res_index);
		nodes[res_index].right = right;
	}
	update_node_info(res_index);
	return res_index;
}
void AdvancedArray::repeat(int num) {
	if (!_size) return;
	std::vector<Variable> all;
	get_all(all);
	if (num < 0) std::reverse(all.begin(), all.end());
	size_t new_size = _size * std::abs(num);
	clear();
	assert(nodes.size() == 1);
	if (new_size) {
		size_t root = _repeat_fill(all, 0, new_size, 0);
		nodes[0].left = root;
	} else nodes[0].left = 0;
	_size = new_size;
}

size_t AdvancedArray::_binary_search(const Variable &var, bool upper_bound) const {
	size_t res = _size;
	size_t cur_node = nodes[0].left;
	if (!cur_node) return 0;
	size_t cur_index = nodes[nodes[cur_node].left].subtree_size;
	while (cur_node) {
		bool too_left = upper_bound ? !(pool[nodes[cur_node].val_index] > var) :
										(pool[nodes[cur_node].val_index] < var);
		if (too_left) { // move right
			cur_node = nodes[cur_node].right;
			cur_index++;
			if (cur_node) cur_index += nodes[nodes[cur_node].left].subtree_size;
		} else {
			res = cur_index;
			cur_node = nodes[cur_node].left;
			cur_index--;
			if (cur_node) cur_index -= nodes[nodes[cur_node].right].subtree_size;
		}
	}
	return res;
}

void AdvancedArray::clear() {
	if (nodes.size() > 1) nodes.erase(nodes.begin() + 1, nodes.end());
	_size = 0;
}

void AdvancedArray::debug(size_t node, size_t level) const {
	for (size_t i = 0; i < level; i++) std::cerr << " ";
	if (level && !node) {
		std::cerr << "none" << std::endl;
		return;
	}
	std::cerr << node << ": size:" << nodes[node].subtree_size << " height:" << nodes[node].subtree_height << " val index:" << nodes[node].val_index;
	std::cerr << " parent:" << nodes[node].parent << std::endl;
	debug(nodes[node].left, level + 1);
	debug(nodes[node].right, level + 1);
}
