#pragma once
#include <cassert>
#include <cstddef>
#include <vector>

struct Variable;

class AdvancedArray {
	struct Node {
		size_t subtree_height = 1;
		size_t subtree_size = 1;
		size_t val_index = -1; // index in internal pool
		size_t parent;
		size_t left = 0;
		size_t right = 0;
		Node (void) : subtree_height(0), subtree_size(0), parent(0) {} // dummy node
		Node (size_t val_index, size_t parent) : val_index(val_index), parent(parent) {}
	};
	std::vector<Node> nodes; // [0] is the dummy root
	size_t _size = 0;
	
	inline int height_sub(size_t node) const {
		size_t left_height = nodes[nodes[node].left].subtree_height;
		size_t right_height = nodes[nodes[node].right].subtree_height;
		return ((int) left_height) - (int) right_height;
	}
	inline void update_node_info(size_t _node) {
		Node &node = nodes[_node];
		node.subtree_height = std::max(nodes[node.left].subtree_height, nodes[node.right].subtree_height) + 1;
		node.subtree_size = nodes[node.left].subtree_size + nodes[node.right].subtree_size + 1;
	}
	inline void replace_subtree(size_t dest, size_t src) {
		size_t parent = nodes[dest].parent;
		if (nodes[parent].left == dest) nodes[parent].left = src;
		else nodes[parent].right = src;
		nodes[src].parent = parent;
	}
	
	// returns the root node of rotated subtree
	inline size_t rotate_l(size_t root) {
		size_t new_root = nodes[root].right;
		assert(new_root);
		replace_subtree(root, new_root);
		nodes[root].right = nodes[new_root].left;
		nodes[nodes[root].right].parent = root;
		nodes[new_root].left = root;
		nodes[root].parent = new_root;
		update_node_info(root);
		update_node_info(new_root);
		return new_root;
	}
	inline size_t rotate_r(size_t root) {
		size_t new_root = nodes[root].left;
		assert(new_root);
		replace_subtree(root, new_root);
		nodes[root].left = nodes[new_root].right;
		nodes[nodes[root].left].parent = root;
		nodes[new_root].right = root;
		nodes[root].parent = new_root;
		update_node_info(root);
		update_node_info(new_root);
		return new_root;
	}
	// double-rotation
	inline size_t rotate_lr(size_t node) {
		rotate_l(nodes[node].left);
		return rotate_r(node);
	}
	inline size_t rotate_rl(size_t node) {
		rotate_r(nodes[node].right);
		return rotate_l(node);
	}
	
	std::vector<Variable> pool;
	
	// balancing
	void balance(size_t node, bool is_remove);
	
	void _get_all(std::vector<Variable> &, size_t node) const;
	void _output(FILE *, size_t node, bool rightest) const;
	
	size_t _init_copy(const Variable *start, const Variable *end, size_t parent);
	size_t _init_fill(size_t size, const Variable &var, size_t parent);
	size_t _repeat_fill(const std::vector<Variable> &, size_t start, size_t end, size_t parent);
	
	size_t _binary_search(const Variable &, bool upper_bound) const;
	
public:
	AdvancedArray(void);
	AdvancedArray(const std::vector<Variable> &);
	AdvancedArray(size_t, const Variable &); // fill
	
	void debug(size_t node = 0, size_t level = 0) const;
	
	void insert(size_t pos, const Variable &);
	void sorted_insert(const Variable &);
	inline void push_back(const Variable &var) { insert(size(), var); }
	void remove_index(size_t pos);
	void repeat(int num);
	void clear(void);
	inline size_t lower_bound(const Variable &var) const { return _binary_search(var, false); }
	inline size_t upper_bound(const Variable &var) const { return _binary_search(var, true); }
	inline size_t size(void) const { return _size; }
	
	void get_all(std::vector<Variable> &) const;
	void output(FILE *stream) const;
	
	Variable &operator [] (size_t index);
	const Variable &operator [] (size_t index) const;
};
